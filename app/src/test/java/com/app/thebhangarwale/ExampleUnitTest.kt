package com.app.thebhangarwale

import android.util.Log
import org.junit.Test

import org.junit.Assert.*
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val input = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val output = SimpleDateFormat("dd/MM/yyyy")

        var d: Date? = null
        try {
            d = input.parse("2021-11-13T11:47:07+0000")
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val formatted: String = output.format(d)
        Log.i("DATE", "" + formatted)
    }

    @Test
    fun test_null_arraylist(){
        val mediaList : ArrayList<String>? = null
        val list = mediaList?.map { media ->
            ""
        } as ArrayList<String>?
        assertNull(list)
    }
}