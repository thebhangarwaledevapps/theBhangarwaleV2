package com.app.thebhangarwale.custom.exception

import java.net.ConnectException

class ServiceUnAvailableException(override var message: String?=null) : ConnectException(message)