package com.app.thebhangarwale.custom.view

import android.content.Context
import android.text.InputType
import android.text.Selection
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import android.text.Spannable
import android.util.AttributeSet
import android.view.KeyEvent

class AutoCompleteTextViewWithoutAutoCloseDropdown : AppCompatAutoCompleteTextView {
    private var adapter: Adapter? = null

    constructor(context: Context) : super(context) {
        inputType = InputType.TYPE_NULL
    }
    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        inputType = InputType.TYPE_NULL
    }
    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attributeSet,
        defStyleAttr
    ) {
        inputType = InputType.TYPE_NULL
    }

    override fun dismissDropDown() {}
    override fun replaceText(text: CharSequence) {
        clearComposingText()
        setText(text)
        val spannable: Spannable = getText()
        Selection.setSelection(spannable, spannable.length)
        super.dismissDropDown()
    }

    override fun onKeyPreIme(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && isPopupShowing) {
            if (event.action == KeyEvent.ACTION_DOWN && event.repeatCount == 0) {
                val state = keyDispatcherState
                state.startTracking(event, this)
                return true
            } else if (event.action == KeyEvent.ACTION_UP) {
                val state = keyDispatcherState
                state.handleUpEvent(event)
                if (event.isTracking && !event.isCanceled) {
                    super.dismissDropDown()
                    return true
                }
            }
        }
        return super.onKeyPreIme(keyCode, event)
    }

    override fun convertSelectionToString(selectedItem: Any): CharSequence {
        if (adapter != null) {
            val convertedItem = adapter!!.getConvert(selectedItem)
            return super.convertSelectionToString(convertedItem)
        }
        return super.convertSelectionToString(selectedItem)
    }

    fun setAdapter(adapter: Adapter?) {
        this.adapter = adapter
    }

    interface Adapter {
        fun getConvert(selectedItem: Any?): Any
    }
}