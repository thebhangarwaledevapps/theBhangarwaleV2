package com.app.thebhangarwale.profile.repository

import com.app.thebhangarwale.login.entity.Customer

interface IProfileRepository {

    @Throws(Exception::class)
    suspend fun getCustomerDetail(): Customer
}