package com.app.thebhangarwale.profile.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.liveData
import com.app.thebhangarwale.login.entity.Customer
import com.app.thebhangarwale.profile.repository.IProfileRepository

class ProfileViewModel(application: Application,val profileRepository: IProfileRepository) : AndroidViewModel(application) {

    fun getCustomerDetail() = liveData<Customer> {
        emit(profileRepository.getCustomerDetail())
    }

}