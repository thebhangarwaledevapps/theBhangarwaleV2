package com.app.thebhangarwale.profile.repository

import com.app.thebhangarwale.contract.disc.shared_pref.CustomerDetailDiscDataSource
import com.app.thebhangarwale.login.entity.Customer
import com.app.thebhangarwale.profile.contract.network.ProfileNetworkDataSource

class ProfileRepositoryImpl(
    val customerDetailDiscDataSource: CustomerDetailDiscDataSource,
    val profileNetworkDataSource: ProfileNetworkDataSource
    ) : IProfileRepository {

    override suspend fun getCustomerDetail(): Customer {
        return customerDetailDiscDataSource.getCustomerDetail()
    }
}