package com.app.thebhangarwale.home.request.view

import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import com.app.thebhangarwale.*
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.exception.NoInternetConnectionException
import com.app.thebhangarwale.custom.view.BhangarwaleSmoothRefreshLayoutHeader
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.app.thebhangarwale.databinding.FragmentCreateRequestBinding
import com.app.thebhangarwale.home.my_account.viewmodel.MyAccountViewModel
import com.app.thebhangarwale.home.request.entity.Item
import com.app.thebhangarwale.home.request.entity.PlaceHolder
import com.app.thebhangarwale.home.request.viewmodel.RequestViewModel
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.transition.MaterialFade
import me.dkzwm.widget.srl.indicator.IIndicator
import javax.inject.Inject

class CreateRequestFragment : Fragment(), View.OnClickListener, Toolbar.OnMenuItemClickListener {

    private val fragmentCreateRequestBinding by lazy {
        activity?.layoutInflater?.let { FragmentCreateRequestBinding.inflate(it) }
    }

    private val onBackPressedCallback by lazy {
        object : OnBackPressedCallback(false) {
            override fun handleOnBackPressed() {
                with(
                    fragmentCreateRequestBinding?.recyclerView?.adapter as RequestAdapter
                ) {
                    selectionTracker?.clearSelection()
                }
            }
        }
    }

    @Inject
    lateinit var requestViewModel: RequestViewModel

    @Inject
    lateinit var myAccountViewModel: MyAccountViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(activity?.application?.let {
                BhangarwaleApplicationModule(
                    it
                )
            })
            .build()
            .injectCreateRequestFragment(this)
        return fragmentCreateRequestBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentCreateRequestBinding?.apply {
            recyclerView.apply {
                adapter = RequestAdapter(this@CreateRequestFragment)
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                addItemDecoration(DividerItemDecoration(requireContext()))
            }.also { recyclerview ->
                with(recyclerview.adapter as RequestAdapter) {
                    selectionTracker = SelectionTracker.Builder<Item>(
                        "item",
                        recyclerview,
                        RequestItemKeyProvider(),
                        object : ItemDetailsLookup<Item>() {
                            override fun getItemDetails(e: MotionEvent): ItemDetails<Item>? =
                                recyclerview.findChildViewUnder(e.x, e.y)?.let { view ->
                                    with(recyclerview.getChildViewHolder(view) as? SelectionViewHolder) {
                                        this?.getRequestItemDetails()
                                    }
                                }
                        },
                        StorageStrategy.createParcelableStorage(Item::class.java)
                    ).build()
                    selectionTracker?.addObserver(object :
                        SelectionTracker.SelectionObserver<Item>() {
                        override fun onItemStateChanged(key: Item, selected: Boolean) {
                            if (selectionTracker?.hasSelection() == true) {
                                onBackPressedCallback.isEnabled = true
                                fragmentCreateRequestBinding?.menuVisibility(View.VISIBLE)
                                selectionTracker?.selection?.size()?.let { count ->
                                    fragmentCreateRequestBinding?.toolbarCountDisplay(
                                        count
                                    )
                                }
                            } else {
                                onBackPressedCallback.isEnabled = false
                                fragmentCreateRequestBinding?.menuVisibility(View.GONE)
                            }
                        }
                    })
                }
            }
            buttonAddItem.setOnClickListener(this@CreateRequestFragment)
            floatingActionButton.setOnClickListener(this@CreateRequestFragment)
            imageViewContinue.setOnClickListener(this@CreateRequestFragment)
            search.roundedCorner.setOnClickListener(this@CreateRequestFragment)
            val header =
                BhangarwaleSmoothRefreshLayoutHeader<IIndicator>(this@CreateRequestFragment.requireActivity())
            mRefreshLayout.setHeaderView(header)
            toolbar.apply {
                inflateMenu(R.menu.menu_delete_item)
                setNavigationOnClickListener {
                    with(fragmentCreateRequestBinding?.recyclerView?.adapter as RequestAdapter) {
                        selectionTracker?.clearSelection()
                    }
                    fragmentCreateRequestBinding?.menuVisibility(View.GONE)
                }
                setOnMenuItemClickListener(this@CreateRequestFragment)
            }
        }
        myAccountViewModel.getCustomerId()?.apply {
            requestViewModel.getRequests(this)
                .observe(viewLifecycleOwner, {
                    when (it) {
                        is BhangarwaleResult.Loading -> {
                            fragmentCreateRequestBinding?.apply {
                                shimmerCreateRequest
                                    .shimmerCreateRequestLayout
                                    .startShimmer()
                            }
                        }
                        is BhangarwaleResult.Success -> {
                            it.data
                            val requestAdapter =
                                fragmentCreateRequestBinding?.recyclerView?.adapter as RequestAdapter
                            requestAdapter.submitList(it.data.items)
                            fragmentCreateRequestBinding?.apply {
                                shimmerCreateRequest
                                    .shimmerCreateRequestLayout
                                    .apply {
                                        visibility = View.GONE
                                        stopShimmer()
                                    }
                                layoutPlaceholderAddItem.apply {
                                    visibility = View.GONE
                                }
                                mRefreshLayout.apply {
                                    visibility = View.VISIBLE
                                }
                                proceedLayout.apply {
                                    visibility = View.VISIBLE
                                }
                                floatingActionButton.apply {
                                    visibility = View.VISIBLE
                                }
                            }
                        }
                        is BhangarwaleResult.Error -> {
                            fragmentCreateRequestBinding?.apply {
                                shimmerCreateRequest
                                    .shimmerCreateRequestLayout
                                    .apply {
                                        visibility = View.GONE
                                        stopShimmer()
                                    }
                            }
                            when(it.exception){
                                is NoInternetConnectionException->{
                                    fragmentCreateRequestBinding?.apply {
                                        layoutPlaceholderNoInternet.apply {
                                            visibility = View.VISIBLE
                                        }
                                    }
                                }
                                else->{
                                    fragmentCreateRequestBinding?.apply {
                                        layoutPlaceholderAddItem.apply {
                                            visibility = View.VISIBLE
                                        }
                                    }

                                }
                            }
                        }

                    }
                })
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            onBackPressedCallback
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.item -> {
                val intent = Intent(activity, UpdateItemActivity::class.java)
                val options = ActivityOptions.makeSceneTransitionAnimation(
                    activity,
                    v,
                    ViewCompat.getTransitionName(v)
                )
                val item: Item = v.getTag(R.string.tag_item) as Item
                intent.putExtra("item", item)
                intent.putExtra("transition_name", ViewCompat.getTransitionName(v))
                startActivity(intent, options.toBundle())
            }
            R.id.floatingActionButton,R.id.buttonAddItem-> {
                startActivity(Intent(activity, AddItemActivity::class.java))
            }
            R.id.imageViewContinue -> {
                startActivity(Intent(activity, ChooseAddressActivity::class.java))
            }
            R.id.rounded_corner -> {
                val intent = Intent(activity, SearchRequestInsideCartActivity::class.java)
                val options = ActivityOptions.makeSceneTransitionAnimation(
                    activity,
                    v,
                    ViewCompat.getTransitionName(v)
                )
                intent.putExtra("transition_name", ViewCompat.getTransitionName(v))
                startActivity(intent, options.toBundle())
            }
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_delete -> {
                activity?.let {
                    MaterialAlertDialogBuilder(it)
                        .setMessage("Delete items?")
                        .setNegativeButton("CANCEL") { dialog, which ->
                        }
                        .setPositiveButton("OK") { dialog, which ->
                            with(
                                fragmentCreateRequestBinding
                                    ?.recyclerView
                                    ?.adapter as RequestAdapter
                            ) {
                                requestViewModel.deleteItemsFromCart(
                                    myAccountViewModel.getCustomerId() as String,
                                    selectionTracker?.selection
                                ).observe(this@CreateRequestFragment, Observer {
                                    when (it) {
                                        is BhangarwaleResult.Success -> {
                                            with(
                                                fragmentCreateRequestBinding
                                                    ?.recyclerView
                                                    ?.adapter as RequestAdapter
                                            ) {
                                                try {
                                                    submitList(it.data.items)
                                                } catch (ex: Exception) {
                                                    println(ex.message)
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                        }
                        .show()
                }
            }
            R.id.navigation_mark_All -> {
                with(
                    fragmentCreateRequestBinding
                        ?.recyclerView
                        ?.adapter as RequestAdapter
                ) {
                    selectAll()
                }
            }

            R.id.navigation_unmark_All -> {
                with(
                    fragmentCreateRequestBinding
                        ?.recyclerView
                        ?.adapter as RequestAdapter
                ) {
                    unSelectAll()
                }
            }
        }
        return true
    }

}

fun FragmentCreateRequestBinding.menuVisibility(visibilityMode: Int) {
    root?.let {
        TransitionManager.beginDelayedTransition(
            it, MaterialFade()
        )
    }
    toolbar?.apply {
        visibility = visibilityMode
    }
}

fun FragmentCreateRequestBinding.toolbarCountDisplay(count: Int) {
    toolbar?.apply {
        title = count.toString()
    }
}

class InfoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

abstract class SelectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun getRequestItemDetails(): ItemDetailsLookup.ItemDetails<Item>?
}

class RequestViewHolder(itemView: View) : SelectionViewHolder(itemView) {
    override fun getRequestItemDetails(): ItemDetailsLookup.ItemDetails<Item>? {
        return object : ItemDetailsLookup.ItemDetails<Item>() {
            override fun getPosition(): Int = itemView.getTag(R.string.tag_position) as Int
            override fun getSelectionKey(): Item? =
                itemView.getTag(R.string.tag_item) as? Item
        }
    }
}

class RequestAdapter(
    val onClickListener: View.OnClickListener,
    private val FOOTER: Int = -1
) : ListAdapter<Item, RecyclerView.ViewHolder>(RequestItemDiffUtilCallBack()) {

    var selectionTracker: SelectionTracker<Item>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            FOOTER -> {
                InfoViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.adapter_info, parent, false)
                )
            }
            else -> {
                RequestViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.adapter_request, parent, false)
                )
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RequestViewHolder -> {
                val item: Item = getItem(position)
                if (item != null) {
                    selectionTracker?.apply {
                        item?.medias?.apply {
                            if (size > 0) {
                                Glide.with(holder.itemView.context)
                                    .loadImage(get(0).itemMediaPath)
                                    .into(holder.itemView.findViewById<AppCompatImageView>(R.id.image))
                            }
                        }
                        holder.itemView.findViewById<AppCompatTextView>(R.id.textview_title).apply {
                            text = item.itemName + " (Rs." + item.itemPrice?.toInt() + "/Kg)"
                        }
                        holder.itemView.findViewById<AppCompatTextView>(R.id.textview_subtitle)
                            .apply {
                                text = "Quantity : " + item.itemQuantity.toString() + " " + "Kg"
                            }
                        holder.itemView.findViewById<AppCompatTextView>(R.id.textview_price_based_on_quantity)
                            .apply {
                                text = item.totalItemPriceForUserAsPerQuantity.toString()
                            }
                        holder.itemView.setTag(R.string.tag_item, getItem(position))
                        holder.itemView.setTag(R.string.tag_position, position)
                        holder.itemView.isActivated = isSelected(getItem(position)) ?: false
                        if (holder.itemView.isActivated) {
                            holder.itemView.findViewById<AppCompatImageView>(R.id.image_activated)
                                .apply {
                                    val backgroundShapeModel: ShapeAppearanceModel =
                                        ShapeAppearanceModel.builder()
                                            .setAllCorners(CornerFamily.ROUNDED, 60.toFloat())
                                            .build()
                                    background = MaterialShapeDrawable(backgroundShapeModel).apply {
                                        fillColor = ColorStateList.valueOf(Color.WHITE)
                                    }
                                    circularRevalV2()
                                }
                        } else {
                            holder.itemView.findViewById<AppCompatImageView>(R.id.image_activated)
                                .apply {
                                    hideReval()
                                }
                        }
                    }
                    ViewCompat.setTransitionName(holder.itemView, position.toString())
                    holder.itemView.setOnClickListener(onClickListener)
                    ViewCompat.setTransitionName(holder.itemView, position.toString())
                }
            }
            is InfoViewHolder -> {
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when(currentList[position]){
            is PlaceHolder->{
                FOOTER
            }
            else->{
                position
            }
        }
    }

    override fun submitList(list: MutableList<Item>?) {
        if(list!=null && list.isNotEmpty()){
            list.add(PlaceHolder())
        }
        super.submitList(list)
    }

    fun selectAll() {
        selectionTracker?.setItemsSelected(currentList, true)
    }

    fun unSelectAll() {
        selectionTracker?.clearSelection()
    }

    inner class RequestItemKeyProvider() : ItemKeyProvider<Item>(ItemKeyProvider.SCOPE_CACHED) {
        override fun getKey(position: Int): Item = getItem(position)
        override fun getPosition(request: Item): Int = currentList.indexOf(request)
    }

    class RequestItemDiffUtilCallBack : ItemCallback<Item>() {
        override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.itemId == newItem.itemId
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem == newItem
        }
    }

}

