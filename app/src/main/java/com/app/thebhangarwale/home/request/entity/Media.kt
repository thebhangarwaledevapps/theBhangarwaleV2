package com.app.thebhangarwale.home.request.entity

import android.net.Uri
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.File
import java.util.*

@Parcelize
open class Media(open val itemId: Long) : Parcelable

@Parcelize
data class ImageUri(override val itemId: Long = Random().nextLong(), val data: Uri) : Media(itemId)

@Parcelize
data class VideoUri(override val itemId: Long = Random().nextLong(), val data: Uri) : Media(itemId)

@Parcelize
data class ImageFromServer(override val itemId: Long, val data: String) : Media(itemId)

@Parcelize
data class VideoFromServer(override val itemId: Long, val data: String) : Media(itemId)

@Parcelize
open class MediaFile(/*open val data: File*/) : Parcelable

@Parcelize
data class ImageFile(/*override*/  val data: File) : MediaFile(/*data*/)

@Parcelize
data class VideoFile(/*override*/  val data: File) : MediaFile(/*data*/)








