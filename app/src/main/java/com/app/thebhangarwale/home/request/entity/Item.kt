package com.app.thebhangarwale.home.request.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*
import kotlin.collections.ArrayList

@Parcelize
open class Item(
    open val itemId: Long? = null,
    val itemName: String? = null,
    val itemQuantity: Int? = null,
    val itemPrice: Double? = null,
    val totalItemPriceForUserAsPerQuantity: Double? = null,
    val medias: ArrayList<ItemMedia>? = null
) : Parcelable

class PlaceHolder(override val itemId: Long?= Long.MIN_VALUE) : Item()