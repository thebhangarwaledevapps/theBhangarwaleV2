package com.app.thebhangarwale.home.feed.repository

import androidx.paging.PagingData
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFacebookFeedInput
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFacebookFeedPosts
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFeedResponse
import kotlinx.coroutines.flow.Flow

interface IFeedRepository {

    @Throws(Exception::class)
    fun getBhangarwaleFacebookFeed(): Flow<PagingData<BhangarwaleFacebookFeedPosts>>
}