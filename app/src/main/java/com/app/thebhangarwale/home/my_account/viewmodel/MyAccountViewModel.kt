package com.app.thebhangarwale.home.my_account.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.liveData
import com.app.thebhangarwale.home.my_account.repository.IMyAccountRepository
import com.app.thebhangarwale.login.entity.Customer

class MyAccountViewModel(application: Application,val myAccountRepository: IMyAccountRepository) : AndroidViewModel(application) {

    fun getCustomerDetail() = liveData<Customer> {
        emit(myAccountRepository.getCustomerDetail())
    }

    fun getCustomerId() = myAccountRepository.getCustomerId()

    /*fun getCustomerId() = "Bhangarwale_00001"*/

}