package com.app.thebhangarwale.home.request.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BhangarTypeAndPriceResponse(
    val bhangarlist: ArrayList<BhangarTypeAndPrice>?,
    val message: String?
) : Parcelable
