package com.app.thebhangarwale.home.request.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
open class BhangarTypeAndPrice(
    val bhangarType: String?=null,
    val bhangarUnit: String?=null,
    val bhangarPrice: Double?=null
) : Parcelable