package com.app.thebhangarwale.home.feed.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.app.thebhangarwale.home.contract.network.AdminNetworkDataSource
import com.app.thebhangarwale.home.feed.contract.network.FeedPagingNetworkDataSourceImpl
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFacebookFeedPosts
import kotlinx.coroutines.flow.Flow

class FeedRepositoryImpl(val adminNetworkDataSource: AdminNetworkDataSource) : IFeedRepository {

    override fun getBhangarwaleFacebookFeed(): Flow<PagingData<BhangarwaleFacebookFeedPosts>> {
        return Pager(
            PagingConfig(10)
        ) {
            FeedPagingNetworkDataSourceImpl(
                adminNetworkDataSource
            )
        }.flow
    }


}