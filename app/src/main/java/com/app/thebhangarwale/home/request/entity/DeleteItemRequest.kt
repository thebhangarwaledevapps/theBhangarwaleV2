package com.app.thebhangarwale.home.request.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DeleteItemRequest(val customerId : String,val items : ArrayList<Item>) : Parcelable
