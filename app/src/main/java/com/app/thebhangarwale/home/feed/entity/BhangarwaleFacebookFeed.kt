package com.app.thebhangarwale.home.feed.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BhangarwaleFacebookFeed (
    val nextPageToken : String?,
    val bhangarwaleFacebookFeedPosts : ArrayList<BhangarwaleFacebookFeedPosts>?
) : Parcelable
