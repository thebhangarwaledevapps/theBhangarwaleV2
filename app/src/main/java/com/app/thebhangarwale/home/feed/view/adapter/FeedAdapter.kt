package com.app.thebhangarwale.home.feed.view.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.viewpager2.widget.ViewPager2
import com.aghajari.zoomhelper.ZoomHelper
import com.app.thebhangarwale.*
import com.app.thebhangarwale.custom.adapter.BhangarwaleOnAttachStateChangeAdapter
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFacebookFeedPosts
import com.app.thebhangarwale.home.feed.entity.Media
import com.app.thebhangarwale.home.feed.view.holder.PagerViewHolder
import com.app.thebhangarwale.home.feed.view.holder.SingleImageViewHolder
import com.app.thebhangarwale.home.feed.view.holder.VideoViewHolder
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import com.hrskrs.instadotlib.InstaDotView

class FeedAdapter(
    val lifeCycleOwner: LifecycleOwner,
    val onClickListener: View.OnClickListener,
    val activity: AppCompatActivity
) : PagingDataAdapter<BhangarwaleFacebookFeedPosts, ViewHolder>(POST_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        lateinit var viewHolder: ViewHolder
        return when (getItem(position)?.postType) {
            "album" -> {
                return PagerViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(
                            R.layout.adapter_pager_feed,
                            parent,
                            false
                        )
                )
            }
            "photo" -> {
                return SingleImageViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(
                            R.layout.adapter_single_image_feed,
                            parent,
                            false
                        )
                )
            }
            "video_inline" -> {
                return VideoViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(
                            R.layout.adapter_video_feed,
                            parent,
                            false
                        ),
                    lifeCycleOwner
                )
            }
            "share" -> {
                return SingleImageViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(
                            R.layout.adapter_single_image_feed,
                            parent,
                            false
                        )
                )
            }
            "profile_media" -> {
                return SingleImageViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(
                            R.layout.adapter_single_image_feed,
                            parent,
                            false
                        )
                )
            }
            "cover_photo" -> {
                return SingleImageViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(
                            R.layout.adapter_single_image_feed,
                            parent,
                            false
                        )
                )
            }
            else -> {
                viewHolder
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bhangarwaleFacebookFeed: BhangarwaleFacebookFeedPosts? = getItem(position)
        when (holder) {
            is VideoViewHolder -> {

                //display post user name
                holder.itemView.findViewById<TextView>(R.id.textview_title).text =
                    bhangarwaleFacebookFeed?.pageTitle

                //display post date
                holder.itemView.findViewById<TextView>(R.id.textview_subtitle).text =
                    bhangarwaleFacebookFeed?.postDate?.convertDateInBhangarwaleStdFormat()

                //display post user image
                Glide.with(holder.itemView.context)
                    .load(bhangarwaleFacebookFeed?.pageImage)
                    .into(holder.itemView.findViewById<AppCompatImageView>(R.id.image))

                //display video
                if (holder.itemView.findViewById<PlayerView>(R.id.video_player).player == null) {
                    bhangarwaleFacebookFeed?.media?.let { singlePhotoList ->
                        if (singlePhotoList?.size > 0) {
                            val media: Media? = singlePhotoList[0];
                            val width = media?.width
                            val height = media?.height
                            if (width != null && height != null) {
                                holder.itemView.findViewById<AppCompatCheckBox>(R.id.imageViewVolume)
                                    .apply {
                                        val shapeAppearanceModel = ShapeAppearanceModel()
                                            .toBuilder()
                                            .setAllCorners(CornerFamily.ROUNDED, 50.toFloat())
                                            .build()
                                        val shapeDrawable =
                                            MaterialShapeDrawable(shapeAppearanceModel)
                                        shapeDrawable.fillColor =
                                            ContextCompat.getColorStateList(
                                                holder.itemView.context,
                                                R.color.color_feed_checkbox
                                            )
                                        ViewCompat.setBackground(this, shapeDrawable)
                                    }
                                SimpleExoPlayer.Builder(holder.itemView.context)
                                    .build()
                                    .apply {
                                        holder.itemView.findViewById<PlayerView>(R.id.video_player).player =
                                            this
                                        holder.itemView.findViewById<PlayerView>(R.id.video_player)
                                            .apply {
                                                layoutParams.height =
                                                    context.getHeightForFeed(width, height)
                                                resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
                                            }
                                        setMediaItem(MediaItem.fromUri(Uri.parse(media.url)))
                                        playWhenReady = true
                                        repeatMode = Player.REPEAT_MODE_ONE
                                        audioComponent?.volume = 0f
                                        prepare()
                                        holder.lifeCycleOwner.lifecycle.addObserver(object :
                                            LifecycleObserver {
                                            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
                                            fun onStop() {
                                                holder.itemView
                                                    .findViewById<AppCompatCheckBox>(R.id.imageViewVolume)
                                                    .isChecked = false
                                                audioComponent?.volume = 0f
                                            }
                                        })
                                        holder.itemView
                                            .findViewById<PlayerView>(R.id.video_player)
                                            .addOnAttachStateChangeListener(object :
                                                BhangarwaleOnAttachStateChangeAdapter() {
                                                override fun onViewDetachedFromWindow(v: View?) {
                                                    holder.itemView
                                                        .findViewById<AppCompatCheckBox>(R.id.imageViewVolume)
                                                        .isChecked = false
                                                    audioComponent?.volume = 0f
                                                }
                                            })
                                        holder.itemView
                                            .findViewById<AppCompatCheckBox>(R.id.imageViewVolume)
                                            .setOnCheckedChangeListener { buttonView, isChecked ->
                                                if (isChecked) {
                                                    audioComponent?.volume = 1f
                                                } else {
                                                    audioComponent?.volume = 0f
                                                }
                                            }
                                    }
                            }
                        }
                    }
                }

                //display text content
                holder.itemView.findViewById<AppCompatTextView>(R.id.textview_content)
                    .apply {
                        bhangarwaleFacebookFeed?.message?.let { addReadMore(it) }
                    }

                holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewPictureInPicture)
                    .apply {
                        setOnClickListener(onClickListener)
                        setTag(holder)
                    }

                holder.itemView.findViewById<AppCompatImageView>(R.id.menu)
                    .setOnClickListener(onClickListener)


            }
            is PagerViewHolder -> {

                //display post user name
                holder.itemView.findViewById<TextView>(R.id.textview_title).text =
                    bhangarwaleFacebookFeed?.pageTitle

                //display post date
                holder.itemView.findViewById<TextView>(R.id.textview_subtitle).text =
                    bhangarwaleFacebookFeed?.postDate?.convertDateInBhangarwaleStdFormat()

                //display post user image
                Glide.with(holder.itemView.context)
                    .load(bhangarwaleFacebookFeed?.pageImage)
                    .into(holder.itemView.findViewById<AppCompatImageView>(R.id.image))

                //display album
                if (holder.itemView.findViewById<ViewPager2>(R.id.viewpager).adapter == null) {
                    holder.itemView.findViewById<ViewPager2>(R.id.viewpager).apply {
                        bhangarwaleFacebookFeed?.media?.let { albumList ->
                            if (albumList?.size > 0) {
                                val width = albumList[0]?.width
                                val height = albumList[0]?.height
                                if (width != null && height != null) {
                                    layoutParams.height = context.getHeightForFeed(width, height)
                                    adapter = MultipleImagePagerAdapter(albumList)
                                    (getChildAt(0) as RecyclerView).overScrollMode =
                                        RecyclerView.OVER_SCROLL_NEVER
                                    registerOnPageChangeCallback(object :
                                        ViewPager2.OnPageChangeCallback() {
                                        override fun onPageSelected(position: Int) {
                                            holder.itemView.findViewById<InstaDotView>(R.id.indicator)
                                                .onPageChange(position)
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
                holder.itemView.findViewById<InstaDotView>(R.id.indicator).apply {
                    var multipleImagePagerAdapter =
                        holder.itemView.findViewById<ViewPager2>(R.id.viewpager).adapter as MultipleImagePagerAdapter
                    noOfPages = multipleImagePagerAdapter.itemCount
                }

                //display text content
                holder.itemView.findViewById<AppCompatTextView>(R.id.textview_content)
                    .apply {
                        bhangarwaleFacebookFeed?.message?.let { addReadMore(it) }
                    }

                //init menu click
                holder.itemView.findViewById<AppCompatImageView>(R.id.menu).apply {
                    setTag(R.string.tag_deep_link, bhangarwaleFacebookFeed?.pageUrl)
                    setOnClickListener(onClickListener)
                }

            }
            is SingleImageViewHolder -> {

                //display post user name
                holder.itemView.findViewById<TextView>(R.id.textview_title).text =
                    bhangarwaleFacebookFeed?.pageTitle

                //display post date
                holder.itemView.findViewById<TextView>(R.id.textview_subtitle).text =
                    bhangarwaleFacebookFeed?.postDate?.convertDateInBhangarwaleStdFormat()

                //display post user image
                Glide.with(holder.itemView.context)
                    .load(bhangarwaleFacebookFeed?.pageImage)
                    .into(holder.itemView.findViewById<AppCompatImageView>(R.id.image))

                bhangarwaleFacebookFeed?.media?.let { singlePhotoList ->
                    if (singlePhotoList?.size > 0) {
                        val media: Media? = singlePhotoList[0];
                        val width = media?.width
                        val height = media?.height
                        if (width != null && height != null) {
                            holder.itemView.findViewById<AppCompatImageView>(R.id.imageview).apply {
                                Glide.with(context)
                                    .load(media?.url)
                                    .into(this)
                                layoutParams.height = context.getHeightForFeed(width, height)
                                ZoomHelper.addZoomableView(this)
                            }
                        }
                    }
                }

                holder.itemView.findViewById<AppCompatTextView>(R.id.textview_content)
                    .apply {
                        bhangarwaleFacebookFeed?.message?.let { addReadMore(it) }
                    }

                holder.itemView.findViewById<AppCompatImageView>(R.id.menu).apply {
                    setTag(R.string.tag_deep_link, bhangarwaleFacebookFeed?.pageUrl)
                    setOnClickListener(onClickListener)
                }

            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    companion object {
        val POST_COMPARATOR = object : DiffUtil.ItemCallback<BhangarwaleFacebookFeedPosts>() {
            override fun areContentsTheSame(
                oldItem: BhangarwaleFacebookFeedPosts,
                newItem: BhangarwaleFacebookFeedPosts
            ): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(
                oldItem: BhangarwaleFacebookFeedPosts,
                newItem: BhangarwaleFacebookFeedPosts
            ): Boolean =
                oldItem.postId.equals(newItem.postId)

        }
    }

}