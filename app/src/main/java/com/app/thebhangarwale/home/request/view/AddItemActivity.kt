package com.app.thebhangarwale.home.request.view

import android.Manifest
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.view.Menu
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.thebhangarwale.*
import com.app.thebhangarwale.RequestCode.REQUEST_IMAGE_GET_FROM_GALLERY
import com.app.thebhangarwale.home.request.viewmodel.MultimediaViewModel
import com.app.thebhangarwale.custom.activity.BhangarwaleConfigAndControllerActivity
import com.app.thebhangarwale.custom.activity_result.BhangarwaleTakePicture
import com.app.thebhangarwale.custom.activity_result.BhangarwaleTakeVideo
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.view.AutoCompleteTextViewWithoutAutoCloseDropdown
import com.app.thebhangarwale.custom.view.ProgressBarDialog
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.app.thebhangarwale.databinding.ActivityAddItemBinding
import com.app.thebhangarwale.home.my_account.viewmodel.MyAccountViewModel
import com.app.thebhangarwale.home.request.entity.*
import com.app.thebhangarwale.home.request.viewmodel.RequestViewModel
import com.bumptech.glide.Glide
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import javax.inject.Inject

@RuntimePermissions
class AddItemActivity : BhangarwaleConfigAndControllerActivity(), View.OnClickListener,
    Toolbar.OnMenuItemClickListener {

    private val multimediaViewModel by lazy {
        MultimediaViewModel(application)
    }

    private val progressBarDialog by lazy {
        ProgressBarDialog(this).show()
    }

    private val activityAddItemBinding: ActivityAddItemBinding by lazy {
        ActivityAddItemBinding.inflate(layoutInflater)
    }

    @Inject
    lateinit var myAccountViewModel: MyAccountViewModel

    @Inject
    lateinit var requestViewModel: RequestViewModel

    private val imageLauncher =
        registerForActivityResult(BhangarwaleTakePicture()) { imageResponse ->
            when (imageResponse.result) {
                RESULT_OK -> {
                    findViewById<RecyclerView>(R.id.recyclerviewImages).apply {
                        val addItemAdapter: AddItemAdapter? = adapter as AddItemAdapter?
                        addItemAdapter?.notifyDataSetChanged(ImageUri(data = imageResponse.uri as Uri))
                    }
                }
            }
        }

    private val videoLauncher = registerForActivityResult(BhangarwaleTakeVideo()) { videoResponse ->
        when (videoResponse.result) {
            RESULT_OK -> {
                findViewById<RecyclerView>(R.id.recyclerviewImages).apply {
                    val addItemAdapter: AddItemAdapter? = adapter as AddItemAdapter?
                    addItemAdapter?.notifyDataSetChanged(VideoUri(data = videoResponse.uri as Uri))
                }
            }
        }
    }

    /*private val onBackPressedCallback by lazy {
        object : OnBackPressedCallback(false) {
            override fun handleOnBackPressed() {
                with(
                    activityAddItemBinding?.recyclerviewImages?.adapter as AddItemAdapter
                ) {
                    selectionTracker?.clearSelection()
                }
            }
        }
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(BhangarwaleApplicationModule(application))
            .build()
            .injectAddItemActivity(this)
        activityAddItemBinding.apply {
            setContentView(root)
            setSupportActionBar(toolbar)
            supportActionBar?.title = resources.getString(R.string.title_add_item)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeButtonEnabled(true)
            toolbarMenu.apply {
                inflateMenu(R.menu.menu_delete_item)
                setNavigationOnClickListener {
                    with(recyclerviewImages?.adapter as AddItemAdapter) {
                        selectionTracker?.clearSelection()
                    }
                    toolbarMenu.visibility = View.GONE
                }
                setOnMenuItemClickListener(this@AddItemActivity)
            }
            recyclerviewImages.apply {
                layoutManager =
                    GridLayoutManager(this@AddItemActivity, 3, GridLayoutManager.VERTICAL, false)
                addItemDecoration(AddItemDecoration())
                adapter = AddItemAdapter(this@AddItemActivity)
            }.also { recyclerviewImages ->
                with(recyclerviewImages.adapter as AddItemAdapter){
                    selectionTracker = SelectionTracker.Builder<Media>(
                        "media",
                        recyclerviewImages,
                        MediaKeyProvider(),
                        object : ItemDetailsLookup<Media>() {
                            override fun getItemDetails(e: MotionEvent): ItemDetails<Media>? =
                                recyclerviewImages.findChildViewUnder(e.x, e.y)?.let { view ->
                                    with(recyclerviewImages.getChildViewHolder(view) as? SelectionMediaViewHolder) {
                                        this?.getMediaDetails()
                                    }
                                }
                        },
                        StorageStrategy.createParcelableStorage(Media::class.java)
                    ).build()
                    selectionTracker?.addObserver(object :
                        SelectionTracker.SelectionObserver<Media>() {
                        override fun onItemStateChanged(key: Media, selected: Boolean) {
                            if (selectionTracker?.hasSelection() == true) {
                                selectionTracker?.selection?.size()?.let { count ->
                                    toolbarMenu.apply {
                                        visibility = View.VISIBLE
                                        title = count.toString()
                                    }
                                }
                            } else {
                                toolbarMenu.apply {
                                    visibility = View.GONE
                                }
                            }
                        }
                    })
                }
            }
            buttonSubmit.setOnClickListener(this@AddItemActivity)
        }
        val bhangarlist: ArrayList<BhangarTypeAndPrice> = ArrayList<BhangarTypeAndPrice>()
        requestViewModel.getBhangarList().observe(this, Observer { response ->
            when (response) {
                is BhangarwaleResult.Loading -> {
                    bhangarlist.add(Progress())
                    activityAddItemBinding.autoCompleteTextViewItems.apply {
                        setAdapter(AutoCompleteAdapter(this@AddItemActivity, bhangarlist))
                    }
                }
                is BhangarwaleResult.Success -> {
                    activityAddItemBinding.autoCompleteTextViewItems.apply {
                        setAdapter(object : AutoCompleteTextViewWithoutAutoCloseDropdown.Adapter {
                            override fun getConvert(selectedItem: Any?): Any {
                                val bhangarTypeAndPrice: BhangarTypeAndPrice =
                                    selectedItem as BhangarTypeAndPrice
                                activityAddItemBinding.autoCompleteTextViewItems.setTag(
                                    R.string.tag_bhangar_item,
                                    bhangarTypeAndPrice
                                )
                                return bhangarTypeAndPrice?.bhangarType + " (Rs." + bhangarTypeAndPrice?.bhangarPrice?.toInt() + "/" + bhangarTypeAndPrice?.bhangarUnit + ")"
                            }
                        })
                        with(adapter as AutoCompleteAdapter) {
                            remove(Progress())
                            response.data.bhangarlist?.let { addAll(it) }
                        }
                    }
                }
                is BhangarwaleResult.Error -> {
                    activityAddItemBinding.autoCompleteTextViewItems.apply {
                        with(adapter as AutoCompleteAdapter) {
                            remove(Progress())
                        }
                    }
                }
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add_item, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_take_photo_or_video_from_gallery -> {
                //selectPhotoFromGallery()
                return true
            }
            R.id.action_take_photo_from_camera -> {
                selectPhotoFromCamera()
                return true
            }
            R.id.action_take_video_from_camera -> {
                selectVideoFromCamera()
                return true
            }
        }
        return false
    }

    override fun navigateUpTo(upIntent: Intent?): Boolean {
        activityAddItemBinding.apply {
            with(recyclerviewImages.adapter as AddItemAdapter) {
                if (selectionTracker?.hasSelection() == true) {
                    selectionTracker?.clearSelection()
                    toolbarMenu.visibility = View.GONE
                } else {
                    upIntent?.putExtra(REDIRECT_SCREEN, R.id.navigation_create_request)
                    startActivity(upIntent)
                    finish()
                }
            }
        }
        return true
    }

    fun selectPhotoFromGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
            type = MIME_TYPE_FOR_IMAGE + ";" + MIME_TYPE_FOR_VIDEO
            addCategory(Intent.CATEGORY_OPENABLE)
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_GET_FROM_GALLERY)
        }
    }

    @SuppressLint("MissingPermission")
    @NeedsPermission(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun selectPhotoFromCamera() {
        imageLauncher.launch(multimediaViewModel.photoUri)
    }

    @SuppressLint("MissingPermission")
    @NeedsPermission(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun selectVideoFromCamera() {
        videoLauncher.launch(multimediaViewModel.videoUri)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    REQUEST_IMAGE_GET_FROM_GALLERY -> {
                        /*&val fullPhotoUri: Uri? = data?.data
                        findViewById<RecyclerView>(R.id.recyclerviewImages).apply {
                            val addItemAdapter: AddItemAdapter? = adapter as AddItemAdapter?
                            addItemAdapter?.notifyDataSetChanged(fullPhotoUri)
                        }*/
                    }
                }

            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imageViewForItems -> {
                when (v.getTag(R.string.tag_image_uri) as Media?) {
                    is ImageUri -> {
                        val intent: Intent = Intent(this, ImageDetailScreenActivity::class.java)
                        val imageObject = v.getTag(R.string.tag_image_uri) as ImageUri
                        intent.data = imageObject.data
                        val options = ViewCompat.getTransitionName(v)?.let {
                            ActivityOptionsCompat.makeSceneTransitionAnimation(
                                this,
                                v as View,
                                it
                            )
                        }
                        startActivity(intent, options?.toBundle())
                    }
                    is VideoUri -> {
                        val intent: Intent = Intent(this, VideoDetailScreenActivity::class.java)
                        val videoObject = v.getTag(R.string.tag_video_uri) as VideoUri
                        intent.data = videoObject.data
                        val options = ViewCompat.getTransitionName(v)?.let {
                            ActivityOptionsCompat.makeSceneTransitionAnimation(
                                this,
                                v as View,
                                it
                            )
                        }
                        startActivity(intent, options?.toBundle())
                    }
                }
            }
            R.id.button_submit -> {
                activityAddItemBinding.apply {
                    val customerId = myAccountViewModel.getCustomerId() as String
                    val bhangarTypeAndPrice =
                        autoCompleteTextViewItems.getTag(R.string.tag_bhangar_item) as? BhangarTypeAndPrice
                    val itemQuantity = textInputEditTextQuantity.text.toString().trim().toInt()
                    val mediaList = with(recyclerviewImages.adapter as AddItemAdapter) { mediaFileList }
                    requestViewModel.addItem(
                        customerId,
                        bhangarTypeAndPrice?.bhangarType,
                        bhangarTypeAndPrice?.bhangarPrice,
                        bhangarTypeAndPrice?.bhangarUnit,
                        itemQuantity,
                        mediaList
                    ).observe(this@AddItemActivity, Observer { result ->
                        when (result) {
                            is BhangarwaleResult.Loading -> {
                                progressBarDialog.show()
                            }
                            is BhangarwaleResult.Success -> {
                                progressBarDialog.dismiss()
                                Toast
                                    .makeText(
                                        this@AddItemActivity,
                                        result.data.message,
                                        Toast.LENGTH_LONG
                                    )
                                    .show()
                            }
                            is BhangarwaleResult.Error -> {
                                progressBarDialog.dismiss()
                                Toast
                                    .makeText(
                                        this@AddItemActivity,
                                        result.exception.message,
                                        Toast.LENGTH_LONG
                                    )
                                    .show()
                            }
                        }
                    })


                }
            }
        }
    }

    override fun onBackPressed() {
        onNavigateUp()
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        return true
    }

}

abstract class SelectionMediaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun getMediaDetails(): ItemDetailsLookup.ItemDetails<Media>?
}

class AddItemDecoration : RecyclerView.ItemDecoration() {

    var spanCount = 3
    var spacing = 16

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view) // item position
        val column = position % spanCount // item column
        outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
        outRect.right =
            spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
    }
}

class AddItemViewHolder(itemView: View) : SelectionMediaViewHolder(itemView){
    override fun getMediaDetails(): ItemDetailsLookup.ItemDetails<Media>? {
        return object : ItemDetailsLookup.ItemDetails<Media>() {
            override fun getPosition(): Int = itemView.getTag(R.string.tag_position) as Int
            override fun getSelectionKey(): Media? =
                itemView.getTag(R.string.tag_item) as? Media
        }
    }

}

class AddItemAdapter(val onClickListener: View.OnClickListener?) :
    RecyclerView.Adapter<AddItemViewHolder>() {

    val mediaFileList: ArrayList<Media> = ArrayList()
    var selectionTracker: SelectionTracker<Media>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddItemViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_add_item, parent, false)
        return AddItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mediaFileList.size ?: 0
    }

    override fun onBindViewHolder(holder: AddItemViewHolder, position: Int) {
        val content: Media? = mediaFileList.get(position)
        selectionTracker?.apply {
            holder.itemView.setTag(R.string.tag_item, content)
            holder.itemView.setTag(R.string.tag_position, position)
            holder.itemView.isActivated = isSelected(content) ?: false
            when (content) {
                is VideoUri -> {
                    holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                        Glide.with(getContext())
                            .load(content.data)
                            .thumbnail(Glide.with(this.context).load(content.data))
                            .into(this)
                        setTag(R.string.tag_video_uri, content)
                        ViewCompat.setTransitionName(holder.itemView, "imageViewForItems")
                        setOnClickListener(onClickListener)
                    }
                }
                is ImageUri -> {
                    holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                        Glide.with(getContext())
                            .load(content.data)
                            .thumbnail(Glide.with(this.context).load(content.data))
                            .into(this)
                        setTag(R.string.tag_image_uri, content)
                        ViewCompat.setTransitionName(holder.itemView, "videoViewForItems")
                        setOnClickListener(onClickListener)
                    }
                }
            }
            if(holder.itemView.isActivated){
                holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                    val objectanimatorX = ObjectAnimator.ofFloat(this,"scaleX",0.75f)
                    val objectanimatorY = ObjectAnimator.ofFloat(this,"scaleY",0.75f)
                    objectanimatorX.duration = 200
                    objectanimatorY.duration = 200
                    objectanimatorX.start();
                    objectanimatorY.start();
                }
                holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewActivated).apply {
                    val backgroundShapeModel: ShapeAppearanceModel =
                        ShapeAppearanceModel.builder()
                            .setAllCorners(CornerFamily.ROUNDED, 60.toFloat())
                            .build()
                    background = MaterialShapeDrawable(backgroundShapeModel).apply {
                        fillColor = ColorStateList.valueOf(Color.WHITE)
                    }
                    visibility = View.VISIBLE
                }
            }else {
                holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                    val objectanimatorX = ObjectAnimator.ofFloat(this, "scaleX", 1f)
                    val objectanimatorY = ObjectAnimator.ofFloat(this, "scaleY", 1f)
                    objectanimatorX.duration = 200
                    objectanimatorY.duration = 200
                    objectanimatorX.start();
                    objectanimatorY.start();
                }
                holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewActivated).apply {
                    visibility = View.INVISIBLE
                }
            }
        }
    }

    fun notifyDataSetChanged(content: Media) {
        if (mediaFileList.size <= 3) {
            mediaFileList.add(content)
            notifyDataSetChanged()
        }
    }

    inner class MediaKeyProvider() : ItemKeyProvider<Media>(ItemKeyProvider.SCOPE_CACHED) {
        override fun getKey(position: Int): Media? = mediaFileList[position]
        override fun getPosition(key: Media): Int = mediaFileList.indexOf(key)
    }
}

class Progress : BhangarTypeAndPrice() {
    override fun equals(other: Any?): Boolean {
        val progress: Progress = other as Progress
        return progress.javaClass.name.equals(this.javaClass.name)
    }
}

class AutoCompleteAdapter(
    context: Context, bhangarlist: ArrayList<BhangarTypeAndPrice>
) : ArrayAdapter<BhangarTypeAndPrice>(context, 0, bhangarlist) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val bhangarTypeAndPrice: BhangarTypeAndPrice? = getItem(position)
        if (bhangarTypeAndPrice is Progress) {
            return LayoutInflater.from(context).inflate(R.layout.dialog_progress_bar, parent, false)
        } else {
            val view: View =
                LayoutInflater.from(context).inflate(R.layout.adapter_bhangar_list, parent, false)
            val textView: TextView = view.findViewById<TextView>(R.id.textView)
            textView.text =
                bhangarTypeAndPrice?.bhangarType + " (Rs." + bhangarTypeAndPrice?.bhangarPrice?.toInt() + "/" + bhangarTypeAndPrice?.bhangarUnit + ")"
            return view
        }
    }

}

