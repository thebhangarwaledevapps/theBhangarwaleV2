package com.app.thebhangarwale.home.feed.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.thebhangarwale.R
import com.app.thebhangarwale.home.feed.entity.Media
import com.app.thebhangarwale.home.feed.view.holder.MultipleImagePagerViewHolder
import com.bumptech.glide.Glide

class MultipleImagePagerAdapter(val albumList: ArrayList<Media?>) : RecyclerView.Adapter<MultipleImagePagerViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MultipleImagePagerViewHolder {
        return MultipleImagePagerViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.adapter_multiple_image_feed,
                    parent,
                    false
                )
        )
    }

    override fun onBindViewHolder(holder: MultipleImagePagerViewHolder, position: Int) {
        val media : Media? = albumList[position]
        Glide.with(holder.itemView.context)
            .load(media?.url)
            .into(holder.itemView.findViewById(R.id.imageView))
    }

    override fun getItemCount(): Int {
        return albumList.size
    }

}
