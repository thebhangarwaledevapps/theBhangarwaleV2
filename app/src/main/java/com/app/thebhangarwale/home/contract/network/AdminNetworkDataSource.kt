package com.app.thebhangarwale.home.contract.network

import com.app.thebhangarwale.home.feed.entity.BhangarwaleFacebookFeedInput
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFeedResponse
import com.app.thebhangarwale.home.request.entity.BhangarTypeAndPriceResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AdminNetworkDataSource {

    @POST("getBhangarwaleFacebookFeed")
    suspend fun getBhangarwaleFacebookFeed(
        @Body bhangarwaleFacebookFeedInput: BhangarwaleFacebookFeedInput
    ): BhangarwaleFeedResponse


    @GET("getAllBhangarTypeAndPrices")
    suspend fun getAllBhangarTypeAndPrices() : BhangarTypeAndPriceResponse

}