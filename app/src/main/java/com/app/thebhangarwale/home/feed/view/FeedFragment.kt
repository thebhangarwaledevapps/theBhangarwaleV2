package com.app.thebhangarwale.home.feed.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import com.app.thebhangarwale.*
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.view.BhangarwaleSmoothRefreshLayoutHeader
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.app.thebhangarwale.databinding.FragmentFeedBinding
import com.app.thebhangarwale.home.feed.view.adapter.FeedAdapter
import com.app.thebhangarwale.home.feed.view.holder.VideoViewHolder
import com.app.thebhangarwale.home.feed.viewmodel.FeedViewModel
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch
import me.dkzwm.widget.srl.indicator.IIndicator
import javax.inject.Inject

class FeedFragment : Fragment(), View.OnClickListener, Toolbar.OnMenuItemClickListener {

    @Inject
    lateinit var feedViewModel: FeedViewModel
    val fragmentFeedBinding by lazy {
        FragmentFeedBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return fragmentFeedBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(activity?.application?.let {
                BhangarwaleApplicationModule(
                    it
                )
            })
            .build()
            .injectFeedFragment(this)
        super.onViewCreated(view, savedInstanceState)
        fragmentFeedBinding.apply {
            toolbar.apply {
                inflateMenu(R.menu.menu_feed)
                setOnMenuItemClickListener(this@FeedFragment)
            }
            recyclerView.apply {
                adapter = FeedAdapter(
                    this@FeedFragment.viewLifecycleOwner,
                    this@FeedFragment,
                    activity as AppCompatActivity
                )
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                addItemDecoration(DividerItemDecoration(requireContext()))
            }
            with(this@FeedFragment.activity) {
                mRefreshLayout.setHeaderView(BhangarwaleSmoothRefreshLayoutHeader<IIndicator>(this))
            }
        }
        feedViewModel.getFeeds().observe(viewLifecycleOwner, {
            with(fragmentFeedBinding.recyclerView.adapter as FeedAdapter) {
                lifecycleScope.launch {
                    submitData(it)
                }
            }
        })
        /*feedViewModel.getFeeds().observe(viewLifecycleOwner, {
            when (it) {
                is BhangarwaleResult.Success -> {
                    fragmentFeedBinding.apply {
                        with(recyclerView.adapter as FeedAdapter){
                            notifyDataSetChanged(
                                it.data?.bhangarwaleFacebookFeed?.bhangarwaleFacebookFeedPosts
                            )
                        }
                        shimmerFeed.root.apply {
                            stopShimmer()
                            visibility = View.GONE
                        }
                        mRefreshLayout.apply {
                            visibility = View.VISIBLE
                        }
                    }
                }
                is BhangarwaleResult.Loading -> {
                    fragmentFeedBinding.apply {
                        shimmerFeed.root.apply {
                            startShimmer()
                        }
                    }
                }
            }
        })*/
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.menu -> {
                context?.let {
                    MaterialAlertDialogBuilder(it)
                        .setItems(feedViewModel.getMenus()) { dialog, which ->
                            when (which) {
                                0 -> {
                                    val deepLink = v.getTag(R.string.tag_deep_link) as String?
                                    deepLink?.let { deepLinkUrl -> startFacebookActivity(deepLinkUrl) }
                                }
                                1 -> {
                                    val deepLink = v.getTag(R.string.tag_deep_link) as String?
                                    startActivity(Intent().apply {
                                        action = Intent.ACTION_SEND
                                        putExtra(
                                            Intent.EXTRA_TEXT,
                                            deepLink
                                        )
                                        type = "text/plain"
                                    })
                                }
                            }
                        }
                        .show()
                }
            }
            R.id.imageViewPictureInPicture -> {
                with(v.getTag() as VideoViewHolder) {
                    itemView.findViewById<PlayerView>(R.id.video_player).player
                        ?.pause()
                }
                startActivity(
                    Intent(requireActivity(), PictureInPictureActivity::class.java).also {
                        it.data =
                            Uri.parse("https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4")
                    }
                )
            }
        }

    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.navigation_share -> {
                //https://play.google.com/store/apps/details?id=com.thekabadiwala.userapp
                startActivity(Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(
                        Intent.EXTRA_TEXT,
                        feedViewModel.shareOurAppUrl
                    )
                    type = "text/html"
                })
                return true
            }
        }
        return false
    }

}





