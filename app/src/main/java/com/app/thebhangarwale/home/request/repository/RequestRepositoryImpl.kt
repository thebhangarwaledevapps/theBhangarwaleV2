package com.app.thebhangarwale.home.request.repository

import com.app.thebhangarwale.home.request.contract.network.RequestNetworkDataSource
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Success
import com.app.thebhangarwale.home.contract.network.AdminNetworkDataSource
import com.app.thebhangarwale.home.request.entity.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

val FORM_DATA = MediaType.parse("multipart/form-data")
val IMG = MediaType.parse("image/*")
val VIDEO = MediaType.parse("video/*")

class RequestRepositoryImpl(
    val requestNetworkDataSource: RequestNetworkDataSource,
    val adminNetworkDataSource: AdminNetworkDataSource
) : IRequestRepository {

    override suspend fun addItemToCart(
        customerId: String,
        bhangarType: String,
        bhangarPrice: Double?,
        bhangarUnit: String?,
        itemQuantity: Int,
        mediaFileList: ArrayList<MediaFile>?
    ): BhangarwaleResult<AddItemCartResponse> {
        val _customerId = RequestBody.create(FORM_DATA, customerId)
        val _bhangarType = RequestBody.create(FORM_DATA, bhangarType)
        val _bhangarUnit = RequestBody.create(FORM_DATA, bhangarUnit.toString())
        val _bhangarPrice = RequestBody.create(FORM_DATA, bhangarPrice.toString())
        val _itemQuantityByUser = RequestBody.create(FORM_DATA, itemQuantity.toString())
        val _mediaList = mediaFileList?.map { media ->
            when (media) {
                is ImageFile -> {
                    MultipartBody.Part.createFormData(
                        "media",
                        media.data.name,
                        RequestBody.create(
                            IMG,
                            media.data
                        ),
                    )
                }
                is VideoFile -> {
                    MultipartBody.Part.createFormData(
                        "media",
                        media.data.name,
                        RequestBody.create(
                            VIDEO,
                            media.data
                        ),
                    )
                }
                else -> {
                }
            }
        } as ArrayList<MultipartBody.Part>?
        return Success(
            requestNetworkDataSource.addItemToCart(
                _customerId,
                _bhangarType,
                _bhangarUnit,
                _bhangarPrice,
                _itemQuantityByUser,
                _mediaList
            )
        )
    }

    override suspend fun getItemsInCart(customerId: String): BhangarwaleResult<ItemResponse> {
        return Success(
            requestNetworkDataSource.getItemsInCart(customerId)
        )
    }

    override suspend fun getAllBhangarTypeAndPrices(): BhangarwaleResult<BhangarTypeAndPriceResponse> {
        return Success(
            adminNetworkDataSource.getAllBhangarTypeAndPrices()
        )
    }

    override suspend fun deleteItemsFromCart(deleteItemRequest: DeleteItemRequest): BhangarwaleResult<ItemResponse> {
        return Success(
            requestNetworkDataSource.deleteItemsFromCart(deleteItemRequest)
        )
    }

    override suspend fun updateItemToCart(
        customerId: String,
        itemId: Long,
        itemQuantity: Int?,
        mediaFileList: ArrayList<MediaFile>?
    ): BhangarwaleResult<UpdateItemCartResponse> {
        val _customerId = RequestBody.create(FORM_DATA, customerId.toString())
        val _itemId = RequestBody.create(FORM_DATA, itemId.toString())
        val _itemQuantity = RequestBody.create(FORM_DATA, itemQuantity.toString())
        val _mediaList = mediaFileList?.map { media ->
            when (media) {
                is ImageFile -> {
                    MultipartBody.Part.createFormData(
                        "media",
                        media.data.name,
                        RequestBody.create(
                            IMG,
                            media.data
                        ),
                    )
                }
                is VideoFile -> {
                    MultipartBody.Part.createFormData(
                        "media",
                        media.data.name,
                        RequestBody.create(
                            VIDEO,
                            media.data
                        ),
                    )
                }
                else -> {
                }
            }
        } as ArrayList<MultipartBody.Part>?
        return Success(
            requestNetworkDataSource.updateItemToCart(
                _customerId,
                _itemId,
                _itemQuantity,
                _mediaList
            )
        )
    }

    override suspend fun deleteImagesFromItem(
        customerId: String,
        itemId: Long,
        arrayList: ArrayList<ItemMedia>
    ): BhangarwaleResult<MediaResponse> {
        return Success(
            requestNetworkDataSource.deleteImagesFromItem(
                DeleteMediaRequest(
                    customerId,
                    itemId,
                    arrayList
                )
            )
        )
    }

}