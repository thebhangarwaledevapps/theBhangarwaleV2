package com.app.thebhangarwale.home.request.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PresentableMediaResponse(val itemMedias: ArrayList<Media>?, val message: String) : Parcelable
