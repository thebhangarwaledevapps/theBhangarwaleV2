package com.app.thebhangarwale.home.request.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Request(
    val id: Long,
    val product: String,
    val quantity: String,
    val price : Float
) : Parcelable