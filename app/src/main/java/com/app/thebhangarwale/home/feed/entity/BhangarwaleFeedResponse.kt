package com.app.thebhangarwale.home.feed.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BhangarwaleFeedResponse(
    val message : String?,
    val bhangarwaleFacebookFeed : BhangarwaleFacebookFeed?
) : Parcelable
