package com.app.thebhangarwale.home.my_account.repository

import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.login.entity.Customer

interface IMyAccountRepository {

    @Throws(Exception::class)
    suspend fun getCustomerDetail(): Customer

    @Throws(Exception::class)
    fun getCustomerId(): String
}