package com.app.thebhangarwale.home.feed.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BhangarwaleFacebookFeedPosts(
    val postId : String?,
    val pageTitle : String?,
    val pageImage : String?,
    val message : String?,
    val postType : String?,
    val media : ArrayList<Media?>?,
    val pageUrl : String?,
    val postDate : String?
) : Parcelable
