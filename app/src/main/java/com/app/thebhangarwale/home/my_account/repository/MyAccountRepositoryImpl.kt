package com.app.thebhangarwale.home.my_account.repository

import com.app.thebhangarwale.contract.disc.shared_pref.CustomerDetailDiscDataSource
import com.app.thebhangarwale.login.entity.Customer

class MyAccountRepositoryImpl(
    val customerDetailDiscDataSource: CustomerDetailDiscDataSource
    ) : IMyAccountRepository {

    override suspend fun getCustomerDetail(): Customer {
        return customerDetailDiscDataSource.getCustomerDetail()
    }

    override fun getCustomerId(): String {
        return customerDetailDiscDataSource.getCustomerId()
    }


}