package com.app.thebhangarwale.home.request.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DeleteMediaRequest(
    val customerId: String,
    val itemId: Long,
    val itemMedias: ArrayList<ItemMedia>
) : Parcelable
