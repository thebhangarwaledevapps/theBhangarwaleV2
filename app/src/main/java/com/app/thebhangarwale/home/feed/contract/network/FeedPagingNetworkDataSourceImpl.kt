package com.app.thebhangarwale.home.feed.contract.network

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.app.thebhangarwale.custom.exception.NoInternetConnectionException
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFacebookFeedInput
import java.net.ConnectException
import androidx.paging.PagingSource.LoadResult.Error
import androidx.paging.PagingSource.LoadResult.Page
import com.app.thebhangarwale.home.contract.network.AdminNetworkDataSource
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFacebookFeedPosts

class FeedPagingNetworkDataSourceImpl(
    val adminNetworkDataSource: AdminNetworkDataSource
) : PagingSource<BhangarwaleFacebookFeedInput, BhangarwaleFacebookFeedPosts>() {

    private var offest: Long = 0

    override suspend fun load(params: LoadParams<BhangarwaleFacebookFeedInput>): LoadResult<BhangarwaleFacebookFeedInput, BhangarwaleFacebookFeedPosts> {
        try {
            var bhangarwaleFacebookFeedInput : BhangarwaleFacebookFeedInput = if(params.key==null){
                BhangarwaleFacebookFeedInput()
            }else{
                params.key as BhangarwaleFacebookFeedInput
            }
            val bhangarwaleFeedResponse =
                adminNetworkDataSource.getBhangarwaleFacebookFeed(bhangarwaleFacebookFeedInput)
            val bhangarwaleFacebookFeed = bhangarwaleFeedResponse?.bhangarwaleFacebookFeed
            val bhangarwaleFacebookFeedPosts = bhangarwaleFacebookFeed?.bhangarwaleFacebookFeedPosts
            if (bhangarwaleFacebookFeedPosts != null) {
                offest += bhangarwaleFacebookFeedPosts.size
                return Page(
                    bhangarwaleFacebookFeedPosts,
                    params.key,
                    BhangarwaleFacebookFeedInput(
                        bhangarwaleFacebookFeed.nextPageToken,
                        offest
                    )
                )
            } else {
                throw Exception()
            }
        } catch (ex: NoInternetConnectionException) {
            return Error(ex)
        } catch (ex: ConnectException) {
            return Error(ex)
        } catch (ex: Exception) {
            return Error(ex)
        }
    }

    override fun getRefreshKey(state: PagingState<BhangarwaleFacebookFeedInput, BhangarwaleFacebookFeedPosts>): BhangarwaleFacebookFeedInput? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey
        }
    }
}