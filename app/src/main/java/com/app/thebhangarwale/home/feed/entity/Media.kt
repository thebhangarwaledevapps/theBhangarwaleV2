package com.app.thebhangarwale.home.feed.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Media(
    val mediaId: String?,
    val url: String?,
    val width: Int?,
    val height: Int?
) : Parcelable
