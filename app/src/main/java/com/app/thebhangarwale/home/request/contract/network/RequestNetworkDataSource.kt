package com.app.thebhangarwale.home.request.contract.network

import com.app.thebhangarwale.home.request.entity.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface RequestNetworkDataSource {

    @Multipart
    @POST("addItemToCart")
    suspend fun addItemToCart(
        @Part("customerId") customerId: RequestBody,
        @Part("bhangarType") bhangarType: RequestBody,
        @Part("bhangarUnit") bhangarUnit: RequestBody,
        @Part("bhangarPrice") bhangarPrice: RequestBody,
        @Part("itemQuantityByUser") itemQuantityByUser: RequestBody,
        @Part media: List<MultipartBody.Part>?
    ): AddItemCartResponse

    @GET("getItemsInCart")
    suspend fun getItemsInCart(
        @Query("customerId") customerId: String
    ): ItemResponse

    @POST("deleteItemsFromCart")
    suspend fun deleteItemsFromCart(@Body deleteItemRequest: DeleteItemRequest): ItemResponse

    @Multipart
    @POST("updateItemToCart")
    suspend fun updateItemToCart(
        @Part("customerId") customerId: RequestBody,
        @Part("itemId") itemId: RequestBody,
        @Part("itemQuantity") itemQuantity: RequestBody?,
        @Part media: List<MultipartBody.Part>?
    ): UpdateItemCartResponse

    @POST("deleteImagesFromItem")
    suspend fun deleteImagesFromItem(@Body deleteMediaRequest: DeleteMediaRequest): MediaResponse

}