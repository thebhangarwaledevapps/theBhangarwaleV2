package com.app.thebhangarwale.home.feed.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.app.thebhangarwale.R
import com.app.thebhangarwale.TheBhangarwaleApplication
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Success
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Loading
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Error
import com.app.thebhangarwale.custom.exception.NoInternetConnectionException
import com.app.thebhangarwale.custom.exception.ServiceUnAvailableException
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFacebookFeedInput
import com.app.thebhangarwale.home.feed.entity.BhangarwaleFeedResponse
import com.app.thebhangarwale.home.feed.repository.IFeedRepository
import kotlinx.coroutines.flow.Flow

class FeedViewModel(application: Application, val feedRepository: IFeedRepository) :
    AndroidViewModel(application) {

    val shareOurAppUrl = "https://play.google.com/store/apps/details?id=com.thekabadiwala.userapp"

    fun getFeeds()  = feedRepository.getBhangarwaleFacebookFeed().asLiveData()

    fun getMenus(): Array<String> {
        return arrayOf(
            getApplication<TheBhangarwaleApplication>().getString(R.string.title_redirect_to_facebook),
            getApplication<TheBhangarwaleApplication>().getString(R.string.title_share_to)
        )
    }


}