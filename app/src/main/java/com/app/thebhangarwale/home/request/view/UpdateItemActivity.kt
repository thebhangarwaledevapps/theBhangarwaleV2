package com.app.thebhangarwale.home.request.view

import android.Manifest
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.app.thebhangarwale.*
import com.app.thebhangarwale.custom.activity_result.BhangarwaleTakePicture
import com.app.thebhangarwale.custom.activity_result.BhangarwaleTakeVideo
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.view.ProgressBarDialog
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.app.thebhangarwale.home.my_account.viewmodel.MyAccountViewModel
import com.app.thebhangarwale.home.request.entity.*
import com.app.thebhangarwale.home.request.viewmodel.MultimediaViewModel
import com.app.thebhangarwale.home.request.viewmodel.RequestViewModel
import com.bumptech.glide.Glide
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.button.MaterialButton
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.transition.platform.MaterialContainerTransform
import com.google.android.material.transition.platform.MaterialContainerTransformSharedElementCallback
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import javax.inject.Inject

@RuntimePermissions
class UpdateItemActivity : LocalizationActivity(), View.OnClickListener,
    Toolbar.OnMenuItemClickListener {

    private val progressBarDialog by lazy {
        ProgressBarDialog(this).show()
    }

    private val multimediaViewModel by lazy {
        MultimediaViewModel(application)
    }

    @Inject
    lateinit var requestViewModel: RequestViewModel

    @Inject
    lateinit var myAccountViewModel: MyAccountViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(BhangarwaleApplicationModule(application))
            .build()
            .injectUpdateItemActivity(this)
        window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
        findViewById<View>(android.R.id.content).transitionName =
            intent.getStringExtra("transition_name")
        setEnterSharedElementCallback(MaterialContainerTransformSharedElementCallback())
        window.sharedElementEnterTransition = MaterialContainerTransform().apply {
            addTarget(android.R.id.content)
            setAllContainerColors(Color.TRANSPARENT)
            //setFadeMode(MaterialContainerTransform.FADE_MODE_THROUGH)
            setScrimColor(resources.getColor(R.color.color_window_background))
            duration = 300L
        }
        window.sharedElementReturnTransition = MaterialContainerTransform().apply {
            addTarget(android.R.id.content)
            setAllContainerColors(Color.TRANSPARENT)
            //setFadeMode(MaterialContainerTransform.FADE_MODE_THROUGH)
            setScrimColor(resources.getColor(R.color.color_window_background))
            duration = 250L
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_item)
        setSupportActionBar(findViewById<MaterialToolbar>(R.id.toolbar))
        supportActionBar?.title = resources.getString(R.string.title_update_item)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        findViewById<MaterialButton>(R.id.button_submit)
            .setOnClickListener(this)
        val toolbarMenu = findViewById<MaterialToolbar>(R.id.toolbarMenu).apply {
            inflateMenu(R.menu.menu_delete_item)
            setOnMenuItemClickListener(this@UpdateItemActivity)
        }
        intent?.apply {
            getParcelableExtra<Item>("item")?.apply {
                findViewById<TextInputEditText>(R.id.textInputEditTextItemName).apply {
                    setText(itemName.toString().trim())
                }
                findViewById<TextInputEditText>(R.id.textInputEditTextQuantity).apply {
                    setText(itemQuantity.toString().trim())
                }
                requestViewModel.convertServerMedia(medias).observe(this@UpdateItemActivity,
                    Observer { mediaList ->
                        if (mediaList != null && mediaList.isNotEmpty()) {
                            findViewById<RecyclerView>(R.id.recyclerviewImages).apply {
                                layoutManager =
                                    GridLayoutManager(
                                        this@UpdateItemActivity,
                                        3,
                                        GridLayoutManager.VERTICAL,
                                        false
                                    )
                                addItemDecoration(UpdateItemDecoration())
                                adapter = UpdateItemAdapter(this@UpdateItemActivity)
                                with(adapter as UpdateItemAdapter) {
                                    selectionTracker = SelectionTracker.Builder<Media>(
                                        "media",
                                        findViewById<RecyclerView>(R.id.recyclerviewImages),
                                        MediaKeyProvider(),
                                        object : ItemDetailsLookup<Media>() {
                                            override fun getItemDetails(e: MotionEvent): ItemDetails<Media>? =
                                                findViewById<RecyclerView>(R.id.recyclerviewImages).findChildViewUnder(
                                                    e.x,
                                                    e.y
                                                )?.let { view ->
                                                    with(
                                                        findViewById<RecyclerView>(R.id.recyclerviewImages).getChildViewHolder(
                                                            view
                                                        ) as? SelectionItemMediaViewHolder
                                                    ) {
                                                        this?.getMediaDetails()
                                                    }
                                                }
                                        },
                                        StorageStrategy.createParcelableStorage(Media::class.java)
                                    ).build()
                                    selectionTracker?.addObserver(object :
                                        SelectionTracker.SelectionObserver<Media>() {
                                        override fun onItemStateChanged(
                                            key: Media,
                                            selected: Boolean
                                        ) {
                                            if (selectionTracker?.hasSelection() == true) {
                                                selectionTracker?.selection?.size()?.let { count ->
                                                    toolbarMenu.apply {
                                                        visibility = View.VISIBLE
                                                        title = count.toString()
                                                    }
                                                }
                                            } else {
                                                toolbarMenu.apply {
                                                    visibility = View.GONE
                                                }
                                            }
                                        }
                                    })
                                    submitList(mediaList)
                                }
                            }
                        }
                    })
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_update_item, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_take_photo_or_video_from_gallery -> {
                //selectPhotoFromGallery()
                return true
            }
            R.id.action_take_photo_from_camera -> {
                with(findViewById<RecyclerView>(R.id.recyclerviewImages).adapter as UpdateItemAdapter) {
                    requestViewModel.canWeOpenCamera(itemCount).observe(this@UpdateItemActivity,
                        Observer {
                            when (it) {
                                is BhangarwaleResult.Success -> {
                                    selectPhotoFromCamera()
                                }
                                is BhangarwaleResult.Error -> {
                                    Toast.makeText(
                                        this@UpdateItemActivity,
                                        it.exception.message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        })

                }
                return true
            }
            R.id.action_take_video_from_camera -> {
                selectVideoFromCamera()
                return true
            }
        }
        return false
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imageViewForItems -> {
                val item = v.getTag(R.string.tag_uri)
                println(item)
                /*if (item.mimeType.startsWith("image")) {
                    val intent = Intent(this, ImageDetailScreenActivity::class.java)
                    intent.data = BhangarwaleUri.parse(item.itemMediaPath)
                    val options = ViewCompat.getTransitionName(v)?.let {
                        ActivityOptionsCompat.makeSceneTransitionAnimation(
                            this,
                            v as View,
                            it
                        )
                    }
                    startActivity(intent, options?.toBundle())
                } else if (item.mimeType.startsWith("video")) {
                    val intent = Intent(this, VideoDetailScreenActivity::class.java)
                    intent.data = BhangarwaleUri.parse(item.itemMediaPath)
                    val options = ViewCompat.getTransitionName(v)?.let {
                        ActivityOptionsCompat.makeSceneTransitionAnimation(
                            this,
                            v as View,
                            it
                        )
                    }
                    startActivity(intent, options?.toBundle())
                }*/
            }
            R.id.button_submit -> {
                try {
                    val item = intent?.getParcelableExtra<Item>("item")
                    val _itemId = item?.itemId as Long
                    val mediaList =
                        with(findViewById<RecyclerView>(R.id.recyclerviewImages).adapter as UpdateItemAdapter) { currentList }
                    val _itemQuantity = findViewById<TextInputEditText>(R.id.textInputEditTextQuantity).text.toString().toInt()
                    requestViewModel.updateItemInCart(
                        myAccountViewModel.getCustomerId() as String,
                        _itemId,
                        _itemQuantity,
                        mediaList
                    ).observe(this, Observer {
                        when(it){
                            is BhangarwaleResult.Success->{

                            }
                        }
                    })
                }catch (ex : Exception){
                    ex.message
                }




                /*requestViewModel.updateItem().observe(this, Observer{
                    when(it){
                        is BhangarwaleResult.Loading->{
                            progressBarDialog.show()
                        }
                        is BhangarwaleResult.Success->{
                            progressBarDialog.dismiss()
                            Toast
                                .makeText(this,it.data, Toast.LENGTH_LONG)
                                .apply {
                                    //setGravity(Gravity.CENTER,0,0)
                                    show()
                                }
                        }
                    }
                })*/
            }
        }
    }

    override fun onBackPressed() {
        with(findViewById<RecyclerView>(R.id.recyclerviewImages).adapter as UpdateItemAdapter) {
            if (selectionTracker?.hasSelection() == true) {
                selectionTracker?.clearSelection()
            } else {
                super.onBackPressed()
            }
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_delete -> {
                with(findViewById<RecyclerView>(R.id.recyclerviewImages).adapter as UpdateItemAdapter) {
                    println(selectionTracker?.selection)
                    requestViewModel.deleteImagesFromItem(
                        myAccountViewModel.getCustomerId() as String,
                        intent.getParcelableExtra<Item>("item")?.itemId as Long,
                        selectionTracker?.selection
                    ).observe(this@UpdateItemActivity, Observer {
                        when (it) {
                            is BhangarwaleResult.Success -> {
                                submitList(it.data.itemMedias)
                            }
                        }
                    })
                }
            }
        }
        return true
    }

    private val imageLauncher =
        registerForActivityResult(BhangarwaleTakePicture()) { imageResponse ->
            when (imageResponse.result) {
                RESULT_OK -> {
                    findViewById<RecyclerView>(R.id.recyclerviewImages).apply {
                        with(adapter as UpdateItemAdapter) {
                            submitItem(ImageUri(data = imageResponse.uri as Uri))
                        }
                    }
                }
            }
        }


    private val videoLauncher = registerForActivityResult(BhangarwaleTakeVideo()) { videoResponse ->
        when (videoResponse.result) {
            RESULT_OK -> {
                findViewById<RecyclerView>(R.id.recyclerviewImages).apply {
                    with(adapter as UpdateItemAdapter) {
                        submitItem(VideoUri(data = videoResponse.uri as Uri))
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @SuppressLint("MissingPermission")
    @NeedsPermission(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun selectPhotoFromCamera() {
        imageLauncher.launch(multimediaViewModel.photoUri)
    }

    @SuppressLint("MissingPermission")
    @NeedsPermission(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun selectVideoFromCamera() {
        videoLauncher.launch(multimediaViewModel.videoUri)
    }

}

abstract class SelectionItemMediaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun getMediaDetails(): ItemDetailsLookup.ItemDetails<Media>
}

class UpdateItemViewHolder(itemView: View) : SelectionItemMediaViewHolder(itemView) {
    override fun getMediaDetails(): ItemDetailsLookup.ItemDetails<Media> {
        return object : ItemDetailsLookup.ItemDetails<Media>() {
            override fun getPosition(): Int = itemView.getTag(R.string.tag_position) as Int
            override fun getSelectionKey(): Media? =
                itemView.getTag(R.string.tag_item) as? Media
        }
    }
}

class UpdateItemDecoration : RecyclerView.ItemDecoration() {

    var spanCount = 3
    var spacing = 16

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view) // item position
        val column = position % spanCount // item column
        outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
        outRect.right =
            spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
    }
}

class UpdateItemAdapter(
    val onClickListener: View.OnClickListener?
) : ListAdapter<Media, UpdateItemViewHolder>(ItemMediaDiffUtilCallBack()) {

    var selectionTracker: SelectionTracker<Media>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpdateItemViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_update_item, parent, false)
        return UpdateItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: UpdateItemViewHolder, position: Int) {
        val content: Media = getItem(position)
        selectionTracker?.apply {
            holder.itemView.setTag(R.string.tag_item, content)
            holder.itemView.setTag(R.string.tag_position, position)
            holder.itemView.isActivated = isSelected(content) ?: false
            when (content) {
                is ImageFromServer -> {
                    holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                        Glide.with(getContext())
                            .loadImage(content.data)
                            .thumbnail(
                                Glide.with(this.context)
                                    .loadImage(content.data)
                            )
                            .into(this)
                        setTag(R.string.tag_uri, content)
                        ViewCompat.setTransitionName(holder.itemView, "imageViewForItems")
                        setOnClickListener(onClickListener)
                    }
                }
                is VideoFromServer -> {
                    holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                        Glide.with(getContext())
                            .loadImage(content.data)
                            .thumbnail(
                                Glide.with(this.context)
                                    .loadImage(content.data)
                            )
                            .into(this)
                        setTag(R.string.tag_uri, content)
                        ViewCompat.setTransitionName(holder.itemView, "videoViewForItems")
                        setOnClickListener(onClickListener)
                    }
                }
                is VideoUri -> {
                    holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                        Glide.with(getContext())
                            .load(content.data)
                            .thumbnail(Glide.with(this.context).load(content.data))
                            .into(this)
                        setTag(R.string.tag_video_uri, content)
                        ViewCompat.setTransitionName(holder.itemView, "imageViewForItems")
                        setOnClickListener(onClickListener)
                    }
                }
                is ImageUri -> {
                    holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                        Glide.with(getContext())
                            .load(content.data)
                            .thumbnail(Glide.with(this.context).load(content.data))
                            .into(this)
                        setTag(R.string.tag_image_uri, content)
                        ViewCompat.setTransitionName(holder.itemView, "videoViewForItems")
                        setOnClickListener(onClickListener)
                    }
                }
            }
            if (holder.itemView.isActivated) {
                holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                    val objectanimatorX = ObjectAnimator.ofFloat(this, "scaleX", 0.75f)
                    val objectanimatorY = ObjectAnimator.ofFloat(this, "scaleY", 0.75f)
                    objectanimatorX.duration = 200
                    objectanimatorY.duration = 200
                    objectanimatorX.start();
                    objectanimatorY.start();
                }
                holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewActivated).apply {
                    val backgroundShapeModel: ShapeAppearanceModel =
                        ShapeAppearanceModel.builder()
                            .setAllCorners(CornerFamily.ROUNDED, 60.toFloat())
                            .build()
                    background = MaterialShapeDrawable(backgroundShapeModel).apply {
                        fillColor = ColorStateList.valueOf(Color.WHITE)
                    }
                    visibility = View.VISIBLE
                }
            } else {
                holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewForItems).apply {
                    val objectanimatorX = ObjectAnimator.ofFloat(this, "scaleX", 1f)
                    val objectanimatorY = ObjectAnimator.ofFloat(this, "scaleY", 1f)
                    objectanimatorX.duration = 200
                    objectanimatorY.duration = 200
                    objectanimatorX.start();
                    objectanimatorY.start();
                }
                holder.itemView.findViewById<AppCompatImageView>(R.id.imageViewActivated).apply {
                    visibility = View.INVISIBLE
                }
            }
        }
    }

    fun submitItem(media: Media) {
        val list = ArrayList<Media>(currentList)
        list.add(media)
        submitList(list)
    }

    inner class MediaKeyProvider() : ItemKeyProvider<Media>(ItemKeyProvider.SCOPE_CACHED) {
        override fun getKey(position: Int): Media? = getItem(position)
        override fun getPosition(key: Media): Int = currentList.indexOf(key)
    }

    class ItemMediaDiffUtilCallBack : DiffUtil.ItemCallback<Media>() {
        override fun areItemsTheSame(oldItem: Media, newItem: Media): Boolean {
            return oldItem.itemId == newItem.itemId
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Media, newItem: Media): Boolean {
            return oldItem == newItem
        }

    }

    /*fun notifyDataSetChanged(content: Media<Uri>) {
        if (mediaList?.size <= 3) {
            mediaList?.add(content)
            notifyDataSetChanged()
        }
    }*/

}

