package com.app.thebhangarwale.home.request.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MediaResponse(val itemMedias: ArrayList<ItemMedia>?, val message: String) : Parcelable

