package com.app.thebhangarwale.home.request.repository

import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.home.request.entity.*

interface IRequestRepository {

    @Throws(Exception::class)
    suspend fun addItemToCart(
        customerId: String,
        bhangarType: String,
        bhangarPrice: Double?,
        bhangarUnit: String?,
        itemQuantity: Int,
        mediaList: ArrayList<MediaFile>?
    ): BhangarwaleResult<AddItemCartResponse>

    @Throws(Exception::class)
    suspend fun getItemsInCart(
        customerId: String
    ): BhangarwaleResult<ItemResponse>

    @Throws(Exception::class)
    suspend fun getAllBhangarTypeAndPrices(
    ): BhangarwaleResult<BhangarTypeAndPriceResponse>

    @Throws(Exception::class)
    suspend fun deleteItemsFromCart(deleteItemRequest: DeleteItemRequest): BhangarwaleResult<ItemResponse>

    @Throws(Exception::class)
    suspend fun updateItemToCart(
        customerId: String,
        itemId: Long,
        itemQuantity: Int?,
        mediaList: ArrayList<MediaFile>?
    ): BhangarwaleResult<UpdateItemCartResponse>

    @Throws(Exception::class)
    suspend fun deleteImagesFromItem(
        customerId: String,
        itemId: Long,
        arrayList: ArrayList<ItemMedia>
    ): BhangarwaleResult<MediaResponse>
}