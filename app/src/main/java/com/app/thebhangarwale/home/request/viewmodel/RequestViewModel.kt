package com.app.thebhangarwale.home.request.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.recyclerview.selection.Selection
import com.app.thebhangarwale.R
import com.app.thebhangarwale.TheBhangarwaleApplication
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Loading
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Error
import com.app.thebhangarwale.custom.exception.BhangarwaleException
import com.app.thebhangarwale.custom.exception.NoInternetConnectionException
import com.app.thebhangarwale.getRealPathFromURI
import com.app.thebhangarwale.home.request.entity.*
import com.app.thebhangarwale.home.request.repository.IRequestRepository
import java.io.File
import java.net.ConnectException

class RequestViewModel(application: Application, val requestRepository: IRequestRepository) :
    AndroidViewModel(application) {

    fun getRequests(customerId: String): LiveData<BhangarwaleResult<ItemResponse>> = liveData {
        try {
            emit(Loading)
            emit(
                requestRepository.getItemsInCart(customerId)
            )
        } catch (ex: NoInternetConnectionException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: ConnectException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: BhangarwaleException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: Exception) {
            emit(
                Error(
                    Exception(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_something_went_wrong)
                    )
                )
            )
        }
    }

    fun addItem(
        customerId: String,
        bhangarType: String?,
        bhangarPrice: Double?,
        bhangarUnit: String?,
        itemQuantity: Int?,
        mediaFileList: ArrayList<Media>?
    ): LiveData<BhangarwaleResult<AddItemCartResponse>> = liveData {
        try {
            emit(Loading)
            if (bhangarType == null || bhangarType.trim().isEmpty()) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_valid_item_selected)
                )
            }
            if (itemQuantity == null) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_valid_quantity_selected)
                )
            }
            emit(
                requestRepository.addItemToCart(
                    customerId,
                    bhangarType.trim(),
                    bhangarPrice,
                    bhangarUnit,
                    itemQuantity,
                    getFiles(mediaFileList)
                )
            )
        } catch (ex: BhangarwaleException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: Exception) {
            println(ex)
            emit(
                Error(
                    Exception(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_something_went_wrong)
                    )
                )
            )
        }
    }

    private fun getFiles(mediaList: ArrayList<Media>?): ArrayList<MediaFile>? {
        return mediaList?.map { media ->
            when (media) {
                is VideoUri -> {
                    VideoFile(
                        File(
                            getApplication<TheBhangarwaleApplication>().getRealPathFromURI(
                                media.data
                            )
                        )
                    )
                }
                is ImageUri -> {
                    ImageFile(
                        File(
                            getApplication<TheBhangarwaleApplication>().getRealPathFromURI(
                                media.data
                            )
                        )
                    )
                }
                else -> {
                }
            }
        } as ArrayList<MediaFile>?
    }

    fun getBhangarList(): LiveData<BhangarwaleResult<BhangarTypeAndPriceResponse>> = liveData {
        try {
            emit(Loading)
            emit(
                requestRepository.getAllBhangarTypeAndPrices()
            )
        } catch (ex: BhangarwaleException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: Exception) {
            emit(
                Error(
                    Exception(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_something_went_wrong)
                    )
                )
            )
        }
    }

    fun deleteItemsFromCart(
        customerId: String,
        selection: Selection<Item>?
    ): LiveData<BhangarwaleResult<ItemResponse>> = liveData {
        try {
            emit(Loading)
            emit(
                requestRepository.deleteItemsFromCart(DeleteItemRequest(customerId, selection?.map {
                    it
                } as ArrayList<Item>))
            )
        } catch (ex: BhangarwaleException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: Exception) {
            emit(
                Error(
                    Exception(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_something_went_wrong)
                    )
                )
            )
        }
    }

    fun updateItemInCart(
        customerId: String,
        itemId: Long,
        itemQuantity: Int?,
        mediaFileList: MutableList<Media>
    ): LiveData<BhangarwaleResult<UpdateItemCartResponse>> = liveData {
        try {
            emit(Loading)
            emit(
                requestRepository.updateItemToCart(
                    customerId,
                    itemId,
                    itemQuantity,
                    getFiles(mediaFileList?.filter {
                        when (it) {
                            is ImageUri -> {
                                true
                            }
                            is VideoUri -> {
                                true
                            }
                            else -> {
                                false
                            }
                        }
                    } as ArrayList<Media>)
                )
            )
        } catch (ex: BhangarwaleException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: Exception) {
            emit(
                Error(
                    Exception(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_something_went_wrong)
                    )
                )
            )
        }
    }

    fun deleteImagesFromItem(customerId: String, itemId: Long, selection: Selection<Media>?):
            LiveData<BhangarwaleResult<PresentableMediaResponse>> = liveData {
        try {
            emit(Loading)
            val result = requestRepository.deleteImagesFromItem(customerId, itemId, selection?.map {
                when (it) {
                    is ImageFromServer -> {
                        ItemMedia(it.itemId, it.data, "image/*")
                    }
                    is VideoFromServer -> {
                        ItemMedia(it.itemId, it.data, "video/*")
                    }
                    else -> {
                    }
                }
            } as ArrayList<ItemMedia>)
            when (result) {
                is BhangarwaleResult.Success -> {
                    result
                    emit(
                        BhangarwaleResult.Success(
                            PresentableMediaResponse(
                                getConvertedServerMediaList(result.data.itemMedias),
                                result.data.message
                            )
                        )
                    )
                }
                is Error -> {
                    emit(Error(result.exception))
                }
            }
        } catch (ex: BhangarwaleException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: Exception) {
            println(ex)
            emit(
                Error(
                    Exception(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_something_went_wrong)
                    )
                )
            )
        }
    }

    fun convertServerMedia(medias: ArrayList<ItemMedia>?): LiveData<ArrayList<Media>> =
        liveData {
            emit(getConvertedServerMediaList(medias))
        }

    private fun getConvertedServerMediaList(medias: ArrayList<ItemMedia>?): ArrayList<Media> {
        return medias?.map {
            when (it.mimeType) {
                "image/*" -> {
                    ImageFromServer(it.itemId, it.itemMediaPath)
                }
                "video/*" -> {
                    VideoFromServer(it.itemId, it.itemMediaPath)
                }
                else -> {
                }
            }
        } as ArrayList<Media>
    }

    fun canWeOpenCamera(itemCount: Int): LiveData<BhangarwaleResult<Nothing?>> = liveData {
        emit(
            if (itemCount > 2) {
                Error(
                    Exception(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_cant_upload)
                    )
                )
            } else {
                BhangarwaleResult.Success(null)
            }
        )
    }

}