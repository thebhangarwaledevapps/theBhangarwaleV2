package com.app.thebhangarwale.home.feed.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BhangarwaleFacebookFeedInput(
    val nextPageAccessToken : String? = null,
    val offset : Long? = 0
) : Parcelable
