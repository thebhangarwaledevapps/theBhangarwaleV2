package com.app.thebhangarwale.home.request.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ItemMedia(
    val itemId: Long,
    val itemMediaPath: String,
    val mimeType: String
) : Parcelable
