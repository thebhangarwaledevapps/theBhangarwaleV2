package com.app.thebhangarwale.login.repository

import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.login.entity.Customer
import com.app.thebhangarwale.login.entity.OTPResponse
import com.app.thebhangarwale.login.entity.PhoneNumberResponse
import java.net.ConnectException

interface ILoginRepository {

    /** Check whether the user is login or not
     * @see LoginRepositoryImpl **/
    suspend fun isUserLogin(): Boolean

    @Throws(Exception::class)
    suspend fun sendPhoneNumberAndFcmTokenServerToGetOtp(
        pincode: String,
        phoneNumber: String,
        fcmToken: String?
    ): BhangarwaleResult<PhoneNumberResponse?>

    @Throws(Exception::class)
    suspend fun saveCustomerIdAndPhoneNumber(
        customerId: String,
        phoneNumber: String,
        countryCode: String
    ): BhangarwaleResult<Customer?>

    @Throws(Exception::class)
    suspend fun verifyOtpAndProceed(
        phoneNumber: String,
        otp: String
    ): BhangarwaleResult<OTPResponse?>


}