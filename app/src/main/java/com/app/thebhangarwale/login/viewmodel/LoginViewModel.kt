package com.app.thebhangarwale.login.viewmodel

import android.app.Application
import android.content.Context
import android.telephony.TelephonyManager
import android.text.TextUtils
import androidx.lifecycle.*
import com.app.thebhangarwale.BuildConfig
import com.app.thebhangarwale.R
import com.app.thebhangarwale.TheBhangarwaleApplication
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Error
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Success
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Loading
import com.app.thebhangarwale.custom.exception.BhangarwaleException
import com.app.thebhangarwale.custom.exception.NoInternetConnectionException
import com.app.thebhangarwale.custom.exception.ServiceUnAvailableException
import com.app.thebhangarwale.login.repository.ILoginRepository
import com.google.firebase.messaging.FirebaseMessaging
import com.google.i18n.phonenumbers.PhoneNumberUtil
import kotlinx.coroutines.tasks.await
import java.net.ConnectException
import kotlin.Exception

const val INDIA = 91

class LoginViewModel(
    application: Application,
    val loginRepository: ILoginRepository
) : AndroidViewModel(application) {

    fun validatedPhoneNumber(
        pinCode: String,
        phoneNumber: String,
        fcmToken: String?
    ): LiveData<BhangarwaleResult<*>> = liveData {
        try {
            emit(Loading)
            if (TextUtils.isEmpty(pinCode.trim())) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_enter_valid_pincode)
                )
            }
            if (!TextUtils.isDigitsOnly(pinCode.trim())) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_enter_valid_pincode)
                )
            }
            if (!pinCode.trim().equals(INDIA.toString().trim())) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_enter_valid_pincode)
                )
            }
            if (TextUtils.isEmpty(phoneNumber.trim())) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_enter_valid_phone_number)
                )
            }
            if (!TextUtils.isDigitsOnly(phoneNumber.trim())) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_enter_valid_phone_number)
                )
            }
            if (!phoneNumber.trim().matches(Regex("\\d{10}"))) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_enter_valid_phone_number)
                )
            }
            emit(
                loginRepository.sendPhoneNumberAndFcmTokenServerToGetOtp(
                    pinCode,
                    phoneNumber,
                    fcmToken
                )
            )
        } catch (ex: BhangarwaleException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: NoInternetConnectionException) {
            ex.message = getApplication<TheBhangarwaleApplication>()
                .getString(R.string.error_please_check_internet_connection)
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: ConnectException) {
            emit(
                Error(
                    ServiceUnAvailableException(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_service_temporarily_unavailable)
                    )
                )
            )
        } catch (ex: Exception) {
            emit(
                Error(
                    Exception(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_something_went_wrong)
                    )
                )
            )
        }
    }

    fun validatedOTP(
        countryCode: String,
        phoneNumber: String,
        otp: String
    ): LiveData<BhangarwaleResult<*>> = liveData {
        try {
            if (TextUtils.isEmpty(otp.trim())) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_enter_valid_otp)
                )
            }
            if (!TextUtils.isDigitsOnly(otp.trim())) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_enter_valid_otp)
                )
            }
            if (!otp.matches(Regex("\\d{6}"))) {
                throw BhangarwaleException(
                    getApplication<TheBhangarwaleApplication>()
                        .resources
                        .getString(R.string.error_enter_valid_otp)
                )
            }
            emit(loginRepository.verifyOtpAndProceed(phoneNumber, otp).also {
                when (it) {
                    is Success -> {
                        it.data?.customerId?.let { customerId ->
                            loginRepository.saveCustomerIdAndPhoneNumber(
                                customerId, phoneNumber, countryCode
                            )
                        }
                    }
                }
            })
        } catch (ex: BhangarwaleException) {
            emit(
                Error(
                    ex
                )
            )
        } catch (ex: Exception) {
            emit(
                Error(
                    Exception(
                        getApplication<TheBhangarwaleApplication>()
                            .getString(R.string.error_something_went_wrong)
                    )
                )
            )
        }
    }

    fun isUserLoggedIn(): LiveData<Boolean> = liveData {
        emit(loginRepository.isUserLogin())
    }

    fun getCurrentCountryCode(): LiveData<BhangarwaleResult<String>> = liveData {
        val countryIso = getCurrentCountryName()
        emit(
            Success(
                data = countryIso + " (+" + PhoneNumberUtil.getInstance()
                    .getCountryCodeForRegion(countryIso).toString() + ")"
            )
        )
    }

    fun getPincodeOnly(): LiveData<BhangarwaleResult<String>> = liveData {
        val countryIso = getCurrentCountryName()
        emit(
            Success(
                data = PhoneNumberUtil.getInstance()
                    .getCountryCodeForRegion(countryIso).toString()
            )
        )
    }

    fun getCurrentCountryName(): String {
        return when (BuildConfig.FLAVOR) {
            "dev" -> {
                val telephonyManager =
                    getApplication<TheBhangarwaleApplication>().getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                telephonyManager.simCountryIso.toUpperCase()
            }
            else -> {
                "IN"
            }
        }
    }

    fun getFirebaseToken(): LiveData<String> = liveData {
        try {
            emit(FirebaseMessaging.getInstance().token.await())
        } catch (ex: Exception) {
            emit("")
            println(ex.message)
        }
    }

}