package com.app.thebhangarwale.login.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class OTPResponse(val customerId : String?,val message : String?) : Parcelable