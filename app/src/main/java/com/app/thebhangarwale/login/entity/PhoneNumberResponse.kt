package com.app.thebhangarwale.login.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PhoneNumberResponse(val message : String?) : Parcelable
