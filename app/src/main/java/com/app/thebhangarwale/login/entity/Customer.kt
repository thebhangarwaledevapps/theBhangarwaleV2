package com.app.thebhangarwale.login.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Customer(
    val customerId: String?=null,
    val customerPhoneNumber: String?=null,
    val customerName: String?=null,
    val customerProfileImage: String?=null,
    val customerCountryCode: String?=null
) : Parcelable