package com.app.thebhangarwale.login.contract.network

import com.app.thebhangarwale.login.entity.OTPResponse
import com.app.thebhangarwale.login.entity.PhoneNumberResponse
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface LoginNetworkDataSource {

    @GET("sendPhoneNumberAndFcmTokenServerToGetOtp")
    suspend fun sendPhoneNumberAndFcmTokenServerToGetOtp(
        @Query("pincode") pincode: String,
        @Query("phoneNumber") phoneNumber: String,
        @Query("fcmToken") fcmToken: String?
    ): PhoneNumberResponse

    @GET("verifyOTPAndProceed")
    suspend fun verifyOtpAndProceed(
        @Query("phoneNumber") phoneNumber: String,
        @Query("otp") otp: String
    ): OTPResponse

}