package com.app.thebhangarwale.login.view

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.view.View
import android.widget.TextView
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.app.thebhangarwale.HomeActivity
import com.app.thebhangarwale.R
import com.app.thebhangarwale.SupportActivity
import com.app.thebhangarwale.custom.adapter.OtpListenerAdapter
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.app.thebhangarwale.databinding.ActivityOtpBinding
import com.app.thebhangarwale.login.viewmodel.LoginViewModel
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Success
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Error
import com.app.thebhangarwale.sms.BhangarwaleBroadcastReceiver
import com.app.thebhangarwale.toCountryCodeFormat
import com.google.android.gms.auth.api.phone.SmsRetriever
import java.util.*
import javax.inject.Inject

class OtpActivity : LocalizationActivity(), View.OnClickListener,
    BhangarwaleBroadcastReceiver.OTPReceiveListener {

    @Inject
    lateinit var loginViewModel: LoginViewModel
    private val activityOtpBinding: ActivityOtpBinding by lazy {
        ActivityOtpBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(BhangarwaleApplicationModule(application))
            .build()
            .injectOtpActivity(this)
        super.onCreate(savedInstanceState)
        setContentView(activityOtpBinding.root)
        intent.apply {
            activityOtpBinding.labelTwo.append(
                String.format(
                    getString(R.string.string_verification_code_subtitle),
                    PhoneNumberUtils.formatNumber(
                        getStringExtra("countryCode")
                            ?.plus(getStringExtra("phoneNumber")?.trim()),
                        loginViewModel.getCurrentCountryName().trim()
                    ).toCountryCodeFormat().trim()
                )
            )
        }
        activityOtpBinding.otpView.otpListener = object : OtpListenerAdapter() {
            override fun onOTPComplete(otp: String) {
                intent.apply {
                    val phoneNumber = getStringExtra("phoneNumber");
                    val countryCode = getStringExtra("countryCode");
                    if (phoneNumber != null && countryCode!=null) {
                        loginViewModel
                            .validatedOTP(countryCode, phoneNumber, otp?.trim())
                            .observe(this@OtpActivity, { result ->
                                when (result) {
                                    is Success -> {
                                        startActivity(
                                            Intent(
                                                this@OtpActivity,
                                                HomeActivity::class.java
                                            ).apply {
                                                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                            })
                                    }
                                    is Error -> {
                                        activityOtpBinding.otpView.showError()
                                    }
                                }
                            })
                    }
                }
            }
        }
        activityOtpBinding.imageviewSupport.setOnClickListener(this)
        SmsRetriever.getClient(this).apply {
            with(startSmsRetriever()) {
                addOnSuccessListener { }
                addOnFailureListener { }
            }
        }
        registerReceiver(BhangarwaleBroadcastReceiver().apply {
            setOTPReceiveListener(this@OtpActivity)
        }, IntentFilter().apply {
            addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imageviewSupport -> {
                startActivity(Intent(this, SupportActivity::class.java))
            }
        }
    }

    override fun onOTPReceived(otp: String) {
        activityOtpBinding.otpView.setOTP(otp.trim())
    }

    override fun onOTPTimeOut() {
    }
}