package com.app.thebhangarwale.login.repository

import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Success
import com.app.thebhangarwale.contract.disc.shared_pref.CustomerDetailDiscDataSource
import com.app.thebhangarwale.login.contract.network.LoginNetworkDataSource
import com.app.thebhangarwale.login.entity.Customer
import com.app.thebhangarwale.login.entity.OTPResponse
import com.app.thebhangarwale.login.entity.PhoneNumberResponse

class LoginRepositoryImpl(
    val loginNetworkDataSource: LoginNetworkDataSource,
    val customerDetailDiscDataSource: CustomerDetailDiscDataSource
) : ILoginRepository {

    override suspend fun isUserLogin(): Boolean {
        return customerDetailDiscDataSource.isUserLogin()
    }

    override suspend fun sendPhoneNumberAndFcmTokenServerToGetOtp(
        pincode: String,
        phoneNumber: String,
        fcmToken: String?
    ): BhangarwaleResult<PhoneNumberResponse?> {
        return Success(
            loginNetworkDataSource
                .sendPhoneNumberAndFcmTokenServerToGetOtp(pincode, phoneNumber, fcmToken)
        )
    }

    override suspend fun saveCustomerIdAndPhoneNumber(
        customerId: String,
        phoneNumber: String,
        countryCode: String
    ): BhangarwaleResult<Customer?> {
        return Success(
            customerDetailDiscDataSource
                .saveCustomerIdAndPhoneNumber(customerId, phoneNumber, countryCode)
        )
    }

    override suspend fun verifyOtpAndProceed(
        phoneNumber: String,
        otp: String
    ): BhangarwaleResult<OTPResponse?> {
        return Success(
            loginNetworkDataSource
                .verifyOtpAndProceed(phoneNumber,otp)
        )
    }

}