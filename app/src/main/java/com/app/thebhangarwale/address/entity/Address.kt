package com.app.thebhangarwale.address.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Address(
    val addressId : Long,
    val customerId : String,
    val address : String,
    val pincode : Int,
    val latLong : LatLong?,
    val landMark : String
) : Parcelable