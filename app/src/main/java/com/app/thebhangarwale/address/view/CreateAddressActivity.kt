package com.app.thebhangarwale.address.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.Observer
import com.app.thebhangarwale.R
import com.app.thebhangarwale.address.viewmodel.AddressViewModel
import com.app.thebhangarwale.custom.activity.BhangarwaleConfigAndControllerActivity
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.view.ProgressBarDialog
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.app.thebhangarwale.home.my_account.viewmodel.MyAccountViewModel
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputEditText
import javax.inject.Inject

class CreateAddressActivity : BhangarwaleConfigAndControllerActivity(), View.OnClickListener {

    private val progressBarDialog by lazy {
        ProgressBarDialog(this).show()
    }

    @Inject
    lateinit var addressViewModel: AddressViewModel

    @Inject
    lateinit var myAccountViewModel: MyAccountViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(BhangarwaleApplicationModule(application))
            .build()
            .injectCreateAddressActivity(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_address)

        setSupportActionBar(findViewById<MaterialToolbar>(R.id.toolbar))
        supportActionBar?.title = resources.getString(R.string.title_create_address)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        findViewById<Button>(R.id.button_submit).setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button_submit -> {
                val customerId = myAccountViewModel.getCustomerId()
                val location =
                    findViewById<TextInputEditText>(R.id.textInputEditTextMyLocation).text.toString()
                        .trim()
                val landMark =
                    findViewById<TextInputEditText>(R.id.textInputEditTextLandMark).text.toString()
                        .trim()
                val pinCode =
                    findViewById<TextInputEditText>(R.id.textInputEditTextPinCode).text.toString()
                        .trim()
                addressViewModel.saveAddressManually(
                    customerId,
                    location,
                    pinCode,
                    landMark
                ).observe(this, Observer {
                    when (it) {
                        is BhangarwaleResult.Loading -> {
                            progressBarDialog?.show()
                        }
                        is BhangarwaleResult.Success -> {
                            progressBarDialog?.dismiss()
                            Toast
                                .makeText(this, it.data?.message, Toast.LENGTH_LONG)
                                .show()
                            startActivity(Intent(this, AddressActivity::class.java))
                        }
                        is BhangarwaleResult.Error->{
                            progressBarDialog?.dismiss()
                            Toast
                                .makeText(this, it.exception.message, Toast.LENGTH_LONG)
                                .show()
                        }
                    }
                })
            }
        }
    }

}