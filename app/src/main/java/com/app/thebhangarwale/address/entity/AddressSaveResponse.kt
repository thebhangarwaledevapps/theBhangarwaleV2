package com.app.thebhangarwale.address.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AddressSaveResponse(val message : String?) : Parcelable