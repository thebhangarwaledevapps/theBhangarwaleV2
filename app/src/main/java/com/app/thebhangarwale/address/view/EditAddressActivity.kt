package com.app.thebhangarwale.address.view

import android.os.Bundle
import com.app.thebhangarwale.EDIT_AVAILABLE_ADDRESS
import com.app.thebhangarwale.R
import com.app.thebhangarwale.address.entity.Address
import com.app.thebhangarwale.custom.activity.BhangarwaleConfigAndControllerActivity
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputEditText

class EditAddressActivity : BhangarwaleConfigAndControllerActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_addres)

        setSupportActionBar(findViewById<MaterialToolbar>(R.id.toolbar))
        supportActionBar?.title = resources.getString(R.string.title_edit_address)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        (intent.getParcelableExtra<Address>(EDIT_AVAILABLE_ADDRESS) as Address).apply {
            findViewById<TextInputEditText>(R.id.textInputEditTextMyLocation).setText(address)
            findViewById<TextInputEditText>(R.id.textInputEditTextLandMark).setText(landMark)
            findViewById<TextInputEditText>(R.id.textInputEditTextPincode).setText(pincode.toString())
        }

    }

}