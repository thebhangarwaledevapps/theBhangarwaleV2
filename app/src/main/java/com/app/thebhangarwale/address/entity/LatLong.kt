package com.app.thebhangarwale.address.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class LatLong(
    val latLongId : Long,
    val latitude : Double,
    val longitude : Double
) : Parcelable