package com.app.thebhangarwale.address.datasource

import android.annotation.SuppressLint
import android.location.Geocoder
import com.app.thebhangarwale.address.contract.network.LocationDataSource
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import android.location.Address

class LocationDataSourceImpl(
    val locationRequest: LocationRequest,
    val fusedLocationProviderClient: FusedLocationProviderClient,
    val geocoder: Geocoder
) : LocationDataSource {

    @SuppressLint("MissingPermission")
    override fun getCurrentLocation() = callbackFlow<Address> {
        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                p0.locations[0].apply {
                    getAddress(latitude, longitude).let { address ->
                        address.latitude = latitude
                        address.longitude = longitude
                        offer(address)
                    }
                }
            }
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
        awaitClose {
            fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        }
    }

    override fun getAddress(latitude: Double, longitude: Double): Address {
        return geocoder.getFromLocation(
            latitude,
            longitude,
            1
        )[0].apply {
            this.latitude = latitude
            this.longitude = longitude
        }
    }

}