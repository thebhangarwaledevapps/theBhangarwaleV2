package com.app.thebhangarwale.address.contract.network

import android.location.Address
import kotlinx.coroutines.flow.Flow

interface LocationDataSource {

    fun getCurrentLocation(): Flow<Address>

    fun getAddress(latitude: Double, longitude: Double): android.location.Address

}