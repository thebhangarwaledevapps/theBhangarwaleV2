package com.app.thebhangarwale.address.entity

import com.google.android.gms.maps.model.LatLng

data class AddressDetail(val latLng: LatLng,val addressLine : String)