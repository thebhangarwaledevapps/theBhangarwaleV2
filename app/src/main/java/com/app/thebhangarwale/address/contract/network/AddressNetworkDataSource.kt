package com.app.thebhangarwale.address.contract.network

import com.app.thebhangarwale.address.entity.AddressResponse
import com.app.thebhangarwale.address.entity.AddressSaveResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface AddressNetworkDataSource {

    @GET("saveAddressWithGoogleMap")
    fun saveAddressWithGoogleMap(
        @Query("customerId") customerId: String,
        @Query("address") address: String,
        @Query("pinCode") pinCode: String?,
        @Query("landMark") landMark: String?,
        @Query("latitude") latitude: Double,
        @Query("longitude") longitude: Double
    ): Call<AddressSaveResponse>

    @GET("getAddress")
    suspend fun getAddresses(@Query("customerId") customerId: String): AddressResponse

    @GET("saveAddress")
    fun saveAddressManually(
        @Query("customerId") customerId: String,
        @Query("address") address: String,
        @Query("pinCode") pinCode: String,
        @Query("landMark") landMark: String
    ): Call<AddressSaveResponse>

}