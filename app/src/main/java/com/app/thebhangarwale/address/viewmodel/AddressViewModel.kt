package com.app.thebhangarwale.address.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.*
import com.app.thebhangarwale.R
import com.app.thebhangarwale.TheBhangarwaleApplication
import com.app.thebhangarwale.address.entity.Address
import com.app.thebhangarwale.address.entity.AddressResponse
import com.app.thebhangarwale.address.entity.AddressSaveResponse
import com.app.thebhangarwale.address.entity.AddressV2
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Error
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Success
import com.app.thebhangarwale.address.repository.IAddressRepository
import com.app.thebhangarwale.custom.exception.BhangarwaleException
import com.app.thebhangarwale.custom.exception.NoInternetConnectionException
import com.app.thebhangarwale.custom.exception.ServiceUnAvailableException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.net.ConnectException
import kotlin.collections.ArrayList

class AddressViewModel(
    application: Application,
    val addressRepository: IAddressRepository
) : AndroidViewModel(application) {

    fun getAddress(customerId: String) = liveData<BhangarwaleResult<AddressResponse>> {
        try {
            emit(BhangarwaleResult.Loading)
            emit(addressRepository.getAddresses(customerId))
        } catch (ex: Exception) {
            emit(Error(ex))
        }
    }

    fun saveAddressWithGoogleMap(
        customerId: String,
        location: String,
        pinCode: String?,
        landMark: String?,
        latitude: Double,
        longitude: Double
    ) = liveData<BhangarwaleResult<AddressSaveResponse?>> {
        withContext(Dispatchers.IO) {
            try {
                emit(BhangarwaleResult.Loading)
                emit(
                    addressRepository.saveAddressWithGoogleMap(
                        customerId,
                        location,
                        pinCode,
                        landMark,
                        latitude,
                        longitude
                    )
                )
            } catch (ex: BhangarwaleException) {
                emit(
                    Error(
                        ex
                    )
                )
            } catch (ex: NoInternetConnectionException) {
                ex.message = getApplication<TheBhangarwaleApplication>()
                    .getString(R.string.error_please_check_internet_connection)
                emit(
                    Error(
                        ex
                    )
                )
            } catch (ex: ConnectException) {
                emit(
                    Error(
                        ServiceUnAvailableException(
                            getApplication<TheBhangarwaleApplication>()
                                .getString(R.string.error_service_temporarily_unavailable)
                        )
                    )
                )
            } catch (ex: Exception) {
                emit(
                    Error(
                        Exception(
                            getApplication<TheBhangarwaleApplication>()
                                .getString(R.string.error_something_went_wrong)
                        )
                    )
                )
            }
        }
    }

    fun saveAddressManually(
        customerId: String,
        location: String,
        pinCode: String,
        landMark: String,
    ) = liveData<BhangarwaleResult<AddressSaveResponse?>> {
        withContext(Dispatchers.IO) {
            try {
                emit(BhangarwaleResult.Loading)
                emit(
                    addressRepository.saveAddressManually(
                        customerId,
                        location,
                        pinCode,
                        landMark
                    )
                )
            }  catch (ex: BhangarwaleException) {
                emit(
                    Error(
                        ex
                    )
                )
            } catch (ex: NoInternetConnectionException) {
                ex.message = getApplication<TheBhangarwaleApplication>()
                    .getString(R.string.error_please_check_internet_connection)
                emit(
                    Error(
                        ex
                    )
                )
            } catch (ex: ConnectException) {
                emit(
                    Error(
                        ServiceUnAvailableException(
                            getApplication<TheBhangarwaleApplication>()
                                .getString(R.string.error_service_temporarily_unavailable)
                        )
                    )
                )
            } catch (ex: Exception) {
                emit(
                    Error(
                        Exception(
                            getApplication<TheBhangarwaleApplication>()
                                .getString(R.string.error_something_went_wrong)
                        )
                    )
                )
            }
        }
    }

    fun updateAddress() = liveData<BhangarwaleResult<String>> {
        try {
            emit(BhangarwaleResult.Loading)
            delay(5000)
            emit(Success(""))
        } catch (ex: Exception) {

        }
    }

    fun getAddress(latitude: Double, longitude: Double) = liveData<android.location.Address?> {
        emit(addressRepository.getAddess(latitude, longitude))
    }

    @SuppressLint("MissingPermission")
    fun getLocation() = addressRepository.getCurrentLocation()

}