package com.app.thebhangarwale.address.view

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentSender
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.MenuItem
import android.view.View
import android.view.animation.BounceInterpolator
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import com.app.thebhangarwale.*
import com.app.thebhangarwale.R
import com.app.thebhangarwale.RequestCode.REQUEST_MY_LOCATION
import com.app.thebhangarwale.address.viewmodel.AddressViewModel
import com.app.thebhangarwale.custom.activity.BhangarwaleConfigAndControllerActivity
import com.app.thebhangarwale.custom.adapter.MotionLayoutTransitionAdapter
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetView
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_INDEFINITE
import com.google.android.material.snackbar.Snackbar
import com.rodolfonavalon.shaperipplelibrary.ShapeRipple
import com.rodolfonavalon.shaperipplelibrary.model.Circle
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import permissions.dispatcher.*
import javax.inject.Inject

@RuntimePermissions
class CreateAddressWithGoogleMapActivity : BhangarwaleConfigAndControllerActivity() {

    @Inject
    lateinit var addressViewModel: AddressViewModel

    @Inject
    lateinit var locationSettingsRequest: LocationSettingsRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(
                BhangarwaleApplicationModule(
                    application
                )
            )
            .build()
            .injectCreateAddressWithGoogleMapActivity(this)
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility =
            (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)

        setContentView(R.layout.activity_create_address_with_google_map)

        findViewById<ShapeRipple>(R.id.ripple).apply {
            rippleDuration = 5757
            rippleCount = 1
            rippleMaximumRadius = 350f
            isEnableSingleRipple = true
            isEnableColorTransition = true
            setRippleColor(resources.getColor(R.color.color_map_ripple))
            rippleShape = Circle()
        }

        setSupportActionBar(findViewById<MaterialToolbar>(R.id.toolbar_view))
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        initLocationWithPermissionCheck()
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    fun onLocationDenied() {
        findViewById<MaterialToolbar>(R.id.toolbar_view)
            .apply {
                inflateMenu(R.menu.menu_after_location_permission_denied)
            }

        TapTargetView.showFor(this,
            TapTarget.forToolbarOverflow(
                findViewById<MaterialToolbar>(R.id.toolbar_view),
                resources.getString(R.string.string_manage_address),
                resources.getString(R.string.string_manage_address_description)
            )
                .outerCircleColor(R.color.color_background_for_hint_view)
                .outerCircleAlpha(0.96f)
                .descriptionTextSize(14)
                .textColor(android.R.color.white)
                .textTypeface(ResourcesCompat.getFont(this, R.font.jet_brains_mono_regular))
                .drawShadow(true)
                .cancelable(true)
                .transparentTarget(true)
                .targetRadius(30),
            object : TapTargetView.Listener() {
                override fun onTargetClick(view: TapTargetView?) {
                    super.onTargetClick(view)
                    findViewById<MaterialToolbar>(R.id.toolbar_view)
                        .showOverflowMenu()
                }
            })

        Snackbar
            .make(
                findViewById<ShapeRipple>(R.id.ripple),
                resources.getString(R.string.string_location_denied),
                Snackbar.LENGTH_LONG
            )
            .setDuration(LENGTH_INDEFINITE)
            .show()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_create_address -> {
                startActivity(Intent(this, CreateAddressActivity::class.java))
                finish()
            }
            R.id.action_system_settings -> {
                startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                    data = Uri.fromParts("package", packageName, null)
                })
                finish()
            }
        }
        return true
    }

    @SuppressLint("MissingPermission")
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun initLocation() {
        LocationServices
            .getSettingsClient(this)
            .checkLocationSettings(locationSettingsRequest)
            .apply {
                addOnSuccessListener {
                    setLocationOnMap()
                }
                addOnFailureListener { exception ->
                    if (exception is ResolvableApiException) {
                        try {
                            exception.startResolutionForResult(
                                this@CreateAddressWithGoogleMapActivity,
                                REQUEST_MY_LOCATION
                            )
                        } catch (sendEx: IntentSender.SendIntentException) {
                        }
                    }
                }
            }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_MY_LOCATION -> {
                when (resultCode) {
                    RESULT_OK -> {
                        setLocationOnMap()
                    }
                    RESULT_CANCELED -> {
                        onLocationDenied()
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun setLocationOnMap() {
        lifecycleScope.launchWhenCreated {
            addressViewModel.getLocation().collect { address ->
                (supportFragmentManager?.apply {
                    with(findFragmentById(R.id.map) as SupportMapFragment) {
                        getMapAsync { googleMap ->
                            googleMap?.apply {
                                findViewById<FloatingActionButton>(R.id.floatingActionButtonMapView).apply {
                                    setOnClickListener {
                                        /*MaterialAlertDialogBuilder(this@CreateAddressWithGoogleMapActivity)
                                            .setItems(arrayOf<String>("Hybrid View","Normal View")) { dialog, which ->
                                                when(which){
                                                    0->{
                                                        googleMap.mapType = GoogleMap.MAP_TYPE_HYBRID
                                                    }
                                                    1->{
                                                        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                                                    }
                                                }
                                                dialog.dismiss()
                                            }
                                            .show()*/
                                    }
                                }
                                val padding = this@CreateAddressWithGoogleMapActivity.dpToPx(6)
                                setPadding(padding, padding, padding, padding)
                                isMyLocationEnabled = true
                                uiSettings?.isMyLocationButtonEnabled = false
                                uiSettings?.isCompassEnabled = false
                                setMapStyle(
                                    MapStyleOptions.loadRawResourceStyle(
                                        activity,
                                        R.raw.style_json
                                    )
                                )
                                animateCamera(
                                    CameraUpdateFactory.newCameraPosition(
                                        CameraPosition.builder()
                                            .target(address?.latitude?.let {
                                                LatLng(
                                                    it,
                                                    address?.longitude
                                                )
                                            })
                                            .zoom(17.8f)
                                            .build()
                                    )
                                )
                                with(findFragmentById(R.id.address) as AddressPickerFragment) {
                                    setOnCameraIdleListener(this)
                                }
                                //marker bounce effect
                                findViewById<MotionLayout>(R.id.motionLayout).apply {
                                    transitionToEnd()
                                    addTransitionListener(object :
                                        MotionLayoutTransitionAdapter() {
                                        override fun onTransitionCompleted(
                                            motionLayout: MotionLayout?,
                                            currentId: Int
                                        ) {
                                            findViewById<AppCompatImageView>(R.id.imageViewPin).apply {
                                                setVisibilityForMotionLayout(View.VISIBLE)
                                                with(animate()) {
                                                    setListener(object : AnimatorListenerAdapter() {
                                                        override fun onAnimationEnd(
                                                            animation: Animator?
                                                        ) {

                                                        }
                                                    })
                                                    setInterpolator(BounceInterpolator())
                                                    setDuration(1000)
                                                    translationYBy(
                                                        resources.getDimensionPixelSize(
                                                            R.dimen.margin_for_map_marker
                                                        ).toFloat()
                                                    )
                                                    start()
                                                }
                                            }
                                            findViewById<ShapeRipple>(R.id.ripple).apply {
                                                setVisibilityForMotionLayout(View.VISIBLE)
                                            }
                                        }
                                    })
                                }
                            }
                        }
                    }
                })
            }
        }
    }
}


