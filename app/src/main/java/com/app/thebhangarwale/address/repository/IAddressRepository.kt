package com.app.thebhangarwale.address.repository

import android.location.Address
import com.app.thebhangarwale.address.entity.AddressResponse
import com.app.thebhangarwale.address.entity.AddressSaveResponse
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import kotlinx.coroutines.flow.Flow

interface IAddressRepository {

    fun getCurrentLocation(): Flow<Address>

    fun getAddess(latitude: Double, longitude: Double): Address

    @Throws(Exception::class)
    suspend fun saveAddressWithGoogleMap(
        customerId: String,
        address: String,
        pinCode: String?,
        landMark: String?,
        latitude: Double,
        longitude: Double
    ): BhangarwaleResult<AddressSaveResponse?>

    @Throws(Exception::class)
    suspend fun getAddresses(customerId: String): BhangarwaleResult<AddressResponse>

    @Throws(Exception::class)
    suspend fun saveAddressManually(
        customerId: String,
        location: String,
        pinCode: String,
        landMark: String
    ): BhangarwaleResult<AddressSaveResponse?>

}