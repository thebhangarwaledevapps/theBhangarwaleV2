package com.app.thebhangarwale.address.repository

import android.location.Address
import com.app.thebhangarwale.address.contract.network.AddressNetworkDataSource
import com.app.thebhangarwale.address.contract.network.LocationDataSource
import com.app.thebhangarwale.address.entity.AddressResponse
import com.app.thebhangarwale.address.entity.AddressSaveResponse
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import kotlinx.coroutines.flow.Flow
import com.app.thebhangarwale.custom.entity.BhangarwaleResult.Success
import com.app.thebhangarwale.custom.exception.BhangarwaleException
import com.app.thebhangarwale.errorMessage

class AddressRepositoryImpl(
    val locationDataSource: LocationDataSource,
    val addressNetworkDataSource: AddressNetworkDataSource
) : IAddressRepository {

    override fun getCurrentLocation(): Flow<Address> = locationDataSource.getCurrentLocation()

    override fun getAddess(latitude: Double, longitude: Double): Address =
        locationDataSource.getAddress(latitude, longitude)

    override suspend fun saveAddressWithGoogleMap(
        customerId: String,
        address: String,
        pinCode: String?,
        landMark: String?,
        latitude: Double,
        longitude: Double
    ): BhangarwaleResult<AddressSaveResponse?> {
        return with(
            addressNetworkDataSource.saveAddressWithGoogleMap(
                customerId, address, pinCode, landMark, latitude, longitude
            ).execute()
        ) {
            when (isSuccessful) {
                true -> {
                    Success(body())
                }
                else -> {
                    throw BhangarwaleException(errorBody()?.errorMessage())
                }
            }
        }
    }

    override suspend fun getAddresses(customerId: String): BhangarwaleResult<AddressResponse> =
        Success(
            addressNetworkDataSource.getAddresses(
                customerId
            )
        )

    override suspend fun saveAddressManually(
        customerId: String,
        location: String,
        pinCode: String,
        landMark: String
    ): BhangarwaleResult<AddressSaveResponse?> {
        return with(
            addressNetworkDataSource.saveAddressManually(
                customerId, location, pinCode, landMark
            ).execute()
        ) {
            when (isSuccessful) {
                true -> {
                    Success(body())
                }
                else -> {
                    throw BhangarwaleException(errorBody()?.errorMessage())
                }
            }
        }
    }
}