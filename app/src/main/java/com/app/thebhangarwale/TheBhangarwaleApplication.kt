package com.app.thebhangarwale

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.akexorcist.localizationactivity.ui.LocalizationApplication
import com.app.thebhangarwale.dagger.component.BhangarwaleAppComponent
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.app.thebhangarwale.dagger.module.BhangarwaleViewModelModule
import com.app.thebhangarwale.settings.viewmodel.SettingsViewModel
import java.util.Locale
import javax.inject.Inject

class TheBhangarwaleApplication : LocalizationApplication() {

    @Inject
    lateinit var settingsViewModel : SettingsViewModel

    override fun getDefaultLanguage(base: Context): Locale {
        return Locale("en")
    }

    override fun onCreate() {
        super.onCreate()
        /*DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(BhangarwaleApplicationModule(this))
            .build()
            .injectTheBhangarwaleApplication(this)
        when (settingsViewModel.getCurrentLanguage()) {
            "light" -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
            "dark" -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }
        }*/
    }

}