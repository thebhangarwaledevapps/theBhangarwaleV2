package com.app.thebhangarwale

import android.animation.Animator
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.view.View
import android.view.ViewAnimationUtils
import android.view.WindowManager
import androidx.constraintlayout.motion.widget.MotionLayout
import java.util.*
import android.content.pm.ApplicationInfo
import android.content.Intent
import androidx.fragment.app.Fragment
import kotlin.math.hypot
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.app.thebhangarwale.custom.network.BhangarwaleNetworkConnectionInterceptor
import okhttp3.OkHttpClient

import android.net.ConnectivityManager
import android.os.Build
import java.text.SimpleDateFormat
import android.provider.MediaStore
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.ViewTreeObserver
import android.widget.TextView
import com.app.thebhangarwale.custom.view.textview.HASH_TAG_PATTERN
import com.app.thebhangarwale.custom.view.textview.PatternEditableBuilder
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.json.JSONObject

fun View.circularReval() {
    addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
        override fun onLayoutChange(
            v: View,
            left: Int,
            top: Int,
            right: Int,
            bottom: Int,
            oldLeft: Int,
            oldTop: Int,
            oldRight: Int,
            oldBottom: Int
        ) {
            v.removeOnLayoutChangeListener(this)
            val anim = ViewAnimationUtils.createCircularReveal(
                v,
                v.right,
                v.bottom,
                0f,
                v.height.toFloat()
            )
            anim.duration = 300
            anim.start()
        }
    })
}

fun Context.getHeightForFeed(width: Int, height: Int): Int {
    return (resources.displayMetrics.widthPixels * height) / width
}

fun View.setVisibilityForMotionLayout(visibility: Int) {
    val motionLayout = parent as MotionLayout
    motionLayout.constraintSetIds.forEach {
        val constraintSet = motionLayout.getConstraintSet(it) ?: return@forEach
        constraintSet.setVisibility(this.id, visibility)
        constraintSet.applyTo(motionLayout)
    }
}

fun Activity.openKeyboard() {
    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
}

fun Activity.hideKeyboard() {
    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
}

fun Activity.startFacebookActivity(facebookUrl: String) {
    var uri: Uri = Uri.parse(facebookUrl)
    try {
        val applicationInfo: ApplicationInfo =
            packageManager.getApplicationInfo("com.facebook.katana", 0)
        if (applicationInfo.enabled) {
            uri = Uri.parse("fb://facewebmodal/f?href=$facebookUrl")
        }
    } catch (ignored: PackageManager.NameNotFoundException) {
    }
    startActivity(Intent(Intent.ACTION_VIEW, uri))
}

fun Fragment.startFacebookActivity(facebookUrl: String) {
    var uri: Uri = Uri.parse(facebookUrl)
    try {
        val applicationInfo: ApplicationInfo? =
            activity?.packageManager?.getApplicationInfo("com.facebook.katana", 0)
        if (applicationInfo?.enabled == true) {
            uri = Uri.parse("fb://facewebmodal/f?href=$facebookUrl")
        }
    } catch (ignored: PackageManager.NameNotFoundException) {
    }
    startActivity(Intent(Intent.ACTION_VIEW, uri))
}

fun View.circularRevalV2() {
    if (visibility == View.INVISIBLE) {
        val cx: Int = width / 2
        val cy: Int = height / 2
        val finalRadius: Double = hypot(cx.toDouble(), cy.toDouble())
        val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, 0f, finalRadius.toFloat())
        visibility = View.VISIBLE
        anim.start()
    }
}

fun View.hideReval() {
    post(Runnable {
        if (visibility == View.VISIBLE) {
            val cx: Int = getWidth() / 2
            val cy: Int = getHeight() / 2
            val initialRadius = hypot(cx.toDouble(), cy.toDouble()).toFloat()
            val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, initialRadius, 0f)
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    visibility = View.INVISIBLE
                }
            })
            anim.start()
        }
    })
}

fun EditText.openKeyBoardWithFocus() {
    requestFocus()
    with(context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?) {
        this?.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }
}

fun OkHttpClient.Builder.buildInternetInterceptor(context: Context): OkHttpClient {
    return addInterceptor(BhangarwaleNetworkConnectionInterceptor(context))
        .build()
}

@SuppressLint("MissingPermission")
fun TheBhangarwaleApplication.isInternetConnected(): Boolean? {
    return with(getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager) {
        activeNetworkInfo?.isConnected
    }
}

fun String.toCountryCodeFormat(): String {
    return "+".plus(this)
}

fun Context.dpToPx(dp: Int): Int {
    return (dp * resources.displayMetrics.density).toInt()
}

fun String.convertDateInBhangarwaleStdFormat(): String {
    val input = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val output = SimpleDateFormat("dd MMM yyyy")
    var d: Date? = input.parse(this)
    return output.format(d)
}

fun Context.getRealPathFromURI(contentUri: Uri): String? {
    val proj = arrayOf(MediaStore.MediaColumns.DATA)
    val cursor = contentResolver.query(contentUri, proj, null, null, null)
    val column_index = cursor?.getColumnIndex(MediaStore.MediaColumns.DATA)
    cursor?.moveToFirst()
    return column_index?.let { cursor.getString(it) }
}

fun TextView.addReadMore(text: String) {
    val isExpand = getTag(R.string.isExpand) as Boolean? ?: false
    if (!isExpand) {
        val count = maxLineToShow(text)
        val isExpectedLength = text.length > count
        val minimumLength = if (isExpectedLength) {
            count
        } else {
            text.length
        }
        if (isExpectedLength) {
            setText(SpannableString(text.substring(0, minimumLength) + "... read more").apply {
                setSpan(object : ClickableSpan() {
                    override fun onClick(view: View) {
                        setText(SpannableString(text).apply {
                            setSpan(null, length - 10, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                        })
                        movementMethod = LinkMovementMethod.getInstance()
                        setTag(R.string.isExpand, true)
                        this@addReadMore.format()
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.isUnderlineText = false
                        ds.color = resources.getColor(R.color.color_clickable)
                    }
                }, length - 10, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            movementMethod = LinkMovementMethod.getInstance()
            format()
        } else {
            setText(SpannableString(text.substring(0, minimumLength)).apply {
                setSpan(null, length - 10, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            movementMethod = LinkMovementMethod.getInstance()
            format()
        }
    }
}

private fun TextView.maxLineToShow(text: String): Int {
    val paint: TextPaint = paint
    val wordwidth = paint.measureText(text, 0, 1)
    val screenwidth = resources.displayMetrics.widthPixels
    return ((screenwidth / wordwidth) * 2).toInt()
}

private fun TextView.format() {
    PatternEditableBuilder()
        .addPattern(
            HASH_TAG_PATTERN,
            resources.getColor(R.color.color_for_social_text)
        )
        .into(this)
}

fun RequestManager.loadImage(path: String): RequestBuilder<Drawable> {
    return load(imgUrlOnFlavourBasis(path))
}

object BhangarwaleUri {
    fun parse(path: String): Uri {
        return Uri.parse(imgUrlOnFlavourBasis(path))
    }
}

private fun imgUrlOnFlavourBasis(path: String) : String{
    return when (BuildConfig.FLAVOR) {
        "dev" -> {
            BuildConfig.IMG_URL + path
        }
        else -> {
            path
        }
    }
}

fun ResponseBody.errorMessage() : String{
    return JSONObject(string()).getString("message")
}



