package com.app.thebhangarwale.settings.repository

interface ISettingsRepository {

    fun getCurrentLanguage(): String?

    fun isNotificationEnabled(): Boolean

}