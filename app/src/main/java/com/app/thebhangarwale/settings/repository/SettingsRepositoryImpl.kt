package com.app.thebhangarwale.settings.repository

import com.app.thebhangarwale.contract.disc.shared_pref.SettingsDiscDataSource

class SettingsRepositoryImpl(val settingsDiscDataSource: SettingsDiscDataSource) : ISettingsRepository {

    override fun getCurrentLanguage(): String? {
        return settingsDiscDataSource.getCurrentLanguage()
    }

    override fun isNotificationEnabled(): Boolean {
        return settingsDiscDataSource.isNotificationEnabled()
    }
}