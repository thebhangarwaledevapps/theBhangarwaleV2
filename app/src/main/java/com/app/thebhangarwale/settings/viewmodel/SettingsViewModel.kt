package com.app.thebhangarwale.settings.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.app.thebhangarwale.settings.repository.ISettingsRepository

class SettingsViewModel(application: Application,val settingsRepository: ISettingsRepository) : AndroidViewModel(application) {

    fun getCurrentLanguage() : String? {
        return settingsRepository.getCurrentLanguage()
    }

    fun isNotificationEnabled() : Boolean {
        return settingsRepository.isNotificationEnabled()
    }

}