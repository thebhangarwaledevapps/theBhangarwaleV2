package com.app.thebhangarwale

import android.os.Build
import android.os.Bundle

class PictureInPictureActivity : VideoDetailScreenActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            enterPictureInPictureMode()
        }
        super.onCreate(savedInstanceState)
    }

    override fun onPictureInPictureModeChanged(isInPictureInPictureMode: Boolean) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode)
        if(!isInPictureInPictureMode){
            finish()
        }
    }


}
