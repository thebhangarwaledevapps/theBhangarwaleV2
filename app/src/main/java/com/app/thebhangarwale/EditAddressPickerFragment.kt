package com.app.thebhangarwale

import android.location.Address
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.app.thebhangarwale.address.viewmodel.AddressViewModel
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.view.ProgressBarDialog
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class EditAddressPickerFragment : Fragment(), View.OnClickListener, GoogleMap.OnCameraIdleListener {

    private lateinit var address: Address
    private val progressBarDialog by lazy {
        activity?.let { ProgressBarDialog(it).show() }
    }

    @Inject
    lateinit var addressViewModel : AddressViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_address_picker,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(activity?.application?.let {
                BhangarwaleApplicationModule(
                    it
                )
            })
            .build()
            .injectEditAddressPickerFragment(this)
        super.onViewCreated(view, savedInstanceState)
        val toolbar : MaterialToolbar = view.findViewById<MaterialToolbar>(R.id.toolbar)
        view.findViewById<MaterialButton>(R.id.button_submit).apply {
            setOnClickListener(this@EditAddressPickerFragment)
        }
        lifecycleScope.launchWhenCreated {
            addressViewModel.getLocation().collect { address ->
                activity?.findViewById<TextInputEditText>(R.id.textInputEditTextLocation)
                    ?.apply {
                        this@EditAddressPickerFragment.address = address
                        setText(address?.getAddressLine(0))
                    }
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.button_submit->{
                /*addressViewModel.submitAddress(
                    customerId,
                    location,
                    pinCode,
                    landMark,
                    latitude,
                    longitude
                ).observe(viewLifecycleOwner, Observer{
                    when(it){
                        is BhangarwaleResult.Loading->{
                            progressBarDialog?.show()
                        }
                        is BhangarwaleResult.Success->{
                            Toast
                                .makeText(activity,it.data, Toast.LENGTH_LONG)
                                .show()
                            activity?.finish()
                        }
                    }
                })*/
            }
        }
    }

    override fun onCameraIdle() {
        activity?.apply {
            supportFragmentManager.apply {
                with(findFragmentById(R.id.map) as SupportMapFragment) {
                    getMapAsync {
                        it.cameraPosition.target.apply {
                            addressViewModel.getAddress(
                                latitude, longitude
                            ).observe(viewLifecycleOwner, { address ->
                                activity?.findViewById<TextInputEditText>(R.id.textInputEditTextLocation)
                                    ?.apply {
                                        if (address != null) {
                                            this@EditAddressPickerFragment.address = address
                                        }
                                        setText(address?.getAddressLine(0))
                                    }
                            })
                        }
                    }
                }
            }
        }
    }


}