package com.app.thebhangarwale

import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.app.thebhangarwale.address.view.CreateAddressActivity
import com.app.thebhangarwale.address.viewmodel.AddressViewModel
import com.app.thebhangarwale.custom.entity.BhangarwaleResult
import com.app.thebhangarwale.custom.view.ProgressBarDialog
import com.app.thebhangarwale.dagger.component.DaggerBhangarwaleAppComponent
import com.app.thebhangarwale.dagger.module.BhangarwaleApplicationModule
import com.app.thebhangarwale.home.my_account.viewmodel.MyAccountViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.flow.collect
import java.util.*
import javax.inject.Inject

class AddressPickerFragment : Fragment(), Toolbar.OnMenuItemClickListener, View.OnClickListener,
    GoogleMap.OnCameraIdleListener {

    private val progressBarDialog by lazy {
        activity?.let { ProgressBarDialog(it).show() }
    }

    @Inject
    lateinit var addressViewModel: AddressViewModel

    @Inject
    lateinit var myAccountViewModel: MyAccountViewModel
    lateinit var address: Address

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_address_picker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        DaggerBhangarwaleAppComponent
            .builder()
            .bhangarwaleApplicationModule(activity?.application?.let {
                BhangarwaleApplicationModule(
                    it
                )
            })
            .build()
            .injectAddressPickerFragment(this)
        super.onViewCreated(view, savedInstanceState)
        val toolbar: MaterialToolbar = view.findViewById<MaterialToolbar>(R.id.toolbar)
        toolbar.inflateMenu(R.menu.menu_create_address)
        toolbar.setOnMenuItemClickListener(this)
        view.findViewById<MaterialButton>(R.id.button_submit).apply {
            setOnClickListener(this@AddressPickerFragment)
        }
        lifecycleScope.launchWhenCreated {
            addressViewModel.getLocation().collect { address ->
                activity?.findViewById<TextInputEditText>(R.id.textInputEditTextLocation)
                    ?.apply {
                        this@AddressPickerFragment.address = address
                        setText(address?.getAddressLine(0))
                    }
            }
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_create_address -> {
                startActivity(Intent(activity, CreateAddressActivity::class.java))
            }
        }
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button_submit -> {
                val customerId = myAccountViewModel.getCustomerId() as String
                val location = address.getAddressLine(0)
                val pinCode = address.postalCode
                val landMark = address.subLocality
                val latitude = address.latitude
                val longitude = address.longitude
                addressViewModel.saveAddressWithGoogleMap(
                    customerId, location, pinCode, landMark, latitude, longitude
                ).observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                    when (it) {
                        is BhangarwaleResult.Loading -> {
                            progressBarDialog?.show()
                        }
                        is BhangarwaleResult.Success -> {
                            Toast
                                .makeText(activity, it.data?.message, Toast.LENGTH_LONG)
                                .show()
                            activity?.finish()
                        }
                        is BhangarwaleResult.Error -> {
                            progressBarDialog?.dismiss()
                            activity
                                ?.findViewById<TextInputLayout>(R.id.textInputLayoutLocation)
                                ?.error = it.exception.message
                        }
                    }
                })

            }
        }
    }

    override fun onCameraIdle() {
        activity?.apply {
            supportFragmentManager.apply {
                with(findFragmentById(R.id.map) as SupportMapFragment) {
                    getMapAsync {
                        it.cameraPosition.target.apply {
                            addressViewModel.getAddress(
                                latitude, longitude
                            ).observe(viewLifecycleOwner, { address ->
                                activity?.findViewById<TextInputEditText>(R.id.textInputEditTextLocation)
                                    ?.apply {
                                        if (address != null) {
                                            this@AddressPickerFragment.address = address
                                        }
                                        setText(address?.getAddressLine(0))
                                    }
                            })
                        }
                    }
                }
            }
        }
    }

}