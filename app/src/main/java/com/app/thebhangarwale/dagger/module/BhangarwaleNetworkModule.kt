package com.app.thebhangarwale.dagger.module

import android.app.Application
import com.app.thebhangarwale.BuildConfig
import com.app.thebhangarwale.address.contract.network.AddressNetworkDataSource
import com.app.thebhangarwale.buildInternetInterceptor
import com.app.thebhangarwale.home.contract.network.AdminNetworkDataSource
import com.app.thebhangarwale.home.request.contract.network.RequestNetworkDataSource
import com.app.thebhangarwale.login.contract.network.LoginNetworkDataSource
import com.app.thebhangarwale.profile.contract.network.ProfileNetworkDataSource
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module(includes = [BhangarwaleApplicationModule::class])
class BhangarwaleNetworkModule {

    @Provides
    fun provideGsonConverterFactory() = GsonConverterFactory.create()

    @Provides
    fun provideClient(application: Application) = OkHttpClient
        .Builder()
        .buildInternetInterceptor(application.applicationContext)

    @Provides
    fun provideAdminNetworkDataSource(
        gsonConverterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ) = Retrofit.Builder()
        .baseUrl(BuildConfig.IP + BuildConfig.ADMIN_BASE_URL)
        .addConverterFactory(gsonConverterFactory)
        .client(okHttpClient)
        .build()
        .create(AdminNetworkDataSource::class.java)

    @Provides
    fun provideLoginNetworkDataSource(
        gsonConverterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ) = Retrofit.Builder()
        .baseUrl(BuildConfig.IP + BuildConfig.LOGIN_BASE_URL)
        .addConverterFactory(gsonConverterFactory)
        .client(okHttpClient)
        .build()
        .create(LoginNetworkDataSource::class.java)

    @Provides
    fun provideProfileNetworkDataSource(
        gsonConverterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ) = Retrofit.Builder()
        .baseUrl(BuildConfig.IP + BuildConfig.LOGIN_BASE_URL)
        .addConverterFactory(gsonConverterFactory)
        .client(okHttpClient)
        .build()
        .create(ProfileNetworkDataSource::class.java)

    @Provides
    fun provideRequestNetworkDataSource(
        gsonConverterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ) = Retrofit.Builder()
        .baseUrl(BuildConfig.IP + BuildConfig.REQUEST_BASE_URL)
        .addConverterFactory(gsonConverterFactory)
        .client(okHttpClient)
        .build()
        .create(RequestNetworkDataSource::class.java)

    @Provides
    fun provideAddressNetworkDataSource(
        gsonConverterFactory: GsonConverterFactory,
        okHttpClient: OkHttpClient
    ) = Retrofit.Builder()
        .baseUrl(BuildConfig.IP + BuildConfig.ADDRESS_BASE_URL)
        .addConverterFactory(gsonConverterFactory)
        .client(okHttpClient)
        .build()
        .create(AddressNetworkDataSource::class.java)

}