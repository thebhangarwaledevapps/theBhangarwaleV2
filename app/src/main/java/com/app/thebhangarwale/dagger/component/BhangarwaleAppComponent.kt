package com.app.thebhangarwale.dagger.component

import com.app.thebhangarwale.*
import com.app.thebhangarwale.address.view.AddressActivity
import com.app.thebhangarwale.address.view.CreateAddressActivity
import com.app.thebhangarwale.address.view.CreateAddressWithGoogleMapActivity
import com.app.thebhangarwale.custom.activity.BhangarwaleConfigAndControllerActivity
import com.app.thebhangarwale.dagger.module.*
import com.app.thebhangarwale.home.feed.view.FeedFragment
import com.app.thebhangarwale.home.my_account.view.MyAccountFragment
import com.app.thebhangarwale.home.request.view.AddItemActivity
import com.app.thebhangarwale.home.request.view.CreateRequestFragment
import com.app.thebhangarwale.home.request.view.UpdateItemActivity
import com.app.thebhangarwale.home.whatsnew.WhatIsNewFragment
import com.app.thebhangarwale.login.view.OtpActivity
import com.app.thebhangarwale.login.view.PhoneNumberActivity
import com.app.thebhangarwale.profile.view.ProfileActivity
import dagger.Component
import javax.inject.Singleton

@Component(modules = [BhangarwaleViewModelModule::class,BhangarwaleLocationModule::class])
interface BhangarwaleAppComponent {

    fun injectPhoneNumberActivity(phoneNumberActivity: PhoneNumberActivity)

    fun injectOtpActivity(otpActivity: OtpActivity)

    fun injectSplashActivity(splashActivity: SplashActivity)

    fun injectBhangarwaleConfigAndControllerActivity(bhangarwaleConfigAndControllerActivity: BhangarwaleConfigAndControllerActivity)

    fun injectFeedFragment(feedFragment: FeedFragment)

    fun injectAddressActivity(addressActivity : AddressActivity)

    fun injectChooseAddressActivity(chooseAddressActivity: ChooseAddressActivity)

    fun injectWhatsNewFragment(whatIsNewFragment: WhatIsNewFragment)

    fun injectCreateAddressActivity(createAddressActivity: CreateAddressActivity)

    fun injectAddressPickerFragment(addressPickerFragment: AddressPickerFragment)

    fun injectEditAddressPickerFragment(editAddressPickerFragment: EditAddressPickerFragment)

    fun injectMyAccountFragment(myAccountFragment: MyAccountFragment)

    fun injectProfileActivity(profileActivity: ProfileActivity)

    fun injectTheBhangarwaleApplication(theBhangarwaleApplication: TheBhangarwaleApplication)

    fun injectBhangarwaleFirebaseMessagingService(bhangarwaleFirebaseMessagingService: BhangarwaleFirebaseMessagingService)

    fun injectCreateAddressWithGoogleMapActivity(createAddressWithGoogleMapActivity : CreateAddressWithGoogleMapActivity)

    fun injectAddItemActivity(addItemActivity: AddItemActivity)

    fun injectCreateRequestFragment(createRequestFragment: CreateRequestFragment)

    fun injectUpdateItemActivity(updateItemActivity: UpdateItemActivity)

}