package com.app.thebhangarwale.dagger.module

import com.app.thebhangarwale.address.contract.network.AddressNetworkDataSource
import com.app.thebhangarwale.address.contract.network.LocationDataSource
import com.app.thebhangarwale.address.repository.AddressRepositoryImpl
import com.app.thebhangarwale.home.feed.repository.FeedRepositoryImpl
import com.app.thebhangarwale.home.my_account.repository.MyAccountRepositoryImpl
import com.app.thebhangarwale.contract.disc.shared_pref.CustomerDetailDiscDataSource
import com.app.thebhangarwale.contract.disc.shared_pref.SettingsDiscDataSource
import com.app.thebhangarwale.home.contract.network.AdminNetworkDataSource
import com.app.thebhangarwale.home.request.contract.network.RequestNetworkDataSource
import com.app.thebhangarwale.home.request.repository.RequestRepositoryImpl
import com.app.thebhangarwale.login.contract.network.LoginNetworkDataSource
import com.app.thebhangarwale.login.repository.LoginRepositoryImpl
import com.app.thebhangarwale.profile.contract.network.ProfileNetworkDataSource
import com.app.thebhangarwale.profile.repository.ProfileRepositoryImpl
import com.app.thebhangarwale.settings.repository.SettingsRepositoryImpl
import dagger.Module
import dagger.Provides

@Module(includes = [BhangarwaleNetworkModule::class, BhangarwaleDiscModule::class, BhangarwaleLocationModule::class])
class BhangarwaleRepositoryModule {

    @Provides
    fun provideFeedRepository(adminNetworkDataSource: AdminNetworkDataSource) =
        FeedRepositoryImpl(adminNetworkDataSource)

    @Provides
    fun provideLoginRepository(
        loginNetworkDataSource: LoginNetworkDataSource,
        customerDetailDiscDataSource: CustomerDetailDiscDataSource
    ) = LoginRepositoryImpl(loginNetworkDataSource, customerDetailDiscDataSource)

    @Provides
    fun provideMyAccountRepository(
        customerDetailDiscDataSource: CustomerDetailDiscDataSource
    ) = MyAccountRepositoryImpl(customerDetailDiscDataSource)

    @Provides
    fun provideProfileRepository(
        customerDetailDiscDataSource: CustomerDetailDiscDataSource,
        profileNetworkDataSource: ProfileNetworkDataSource
    ) = ProfileRepositoryImpl(customerDetailDiscDataSource, profileNetworkDataSource)

    @Provides
    fun provideSettingsRepository(
        settingsDiscDataSource: SettingsDiscDataSource
    ) = SettingsRepositoryImpl(settingsDiscDataSource)

    @Provides
    fun provideRequestRepository(
        requestNetworkDataSource: RequestNetworkDataSource,
        adminNetworkDataSource: AdminNetworkDataSource
    ) = RequestRepositoryImpl(requestNetworkDataSource,adminNetworkDataSource)

    @Provides
    fun provideAddressRepository(
        locationDataSource: LocationDataSource,
        addressNetworkDataSource: AddressNetworkDataSource
    ) = AddressRepositoryImpl(locationDataSource,addressNetworkDataSource)

}