package com.app.thebhangarwale.dagger.module

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.app.thebhangarwale.contract.disc.shared_pref.CustomerDetailDiscDataSource
import com.app.thebhangarwale.contract.disc.shared_pref.CustomerDetailDiscDataSourceImpl
import com.app.thebhangarwale.contract.disc.shared_pref.SettingsDiscDataSource
import com.app.thebhangarwale.contract.disc.shared_pref.SettingsDiscDataSourceImpl
import dagger.Module
import dagger.Provides

@Module(includes = [BhangarwaleApplicationModule::class])
class BhangarwaleDiscModule {

    @Provides
    fun provideLoginDiscDataSource(application : Application) : CustomerDetailDiscDataSource =
        CustomerDetailDiscDataSourceImpl(
            EncryptedSharedPreferences.create(
                "theBhangarwaleSharedPref",
                MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                application.applicationContext,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
        )

    @Provides
    fun provideSettingsDiscDataSource(application: Application) : SettingsDiscDataSource =
        SettingsDiscDataSourceImpl(
            PreferenceManager
                .getDefaultSharedPreferences(application)
        )

}