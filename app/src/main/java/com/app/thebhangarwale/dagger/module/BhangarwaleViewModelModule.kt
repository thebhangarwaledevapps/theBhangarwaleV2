package com.app.thebhangarwale.dagger.module

import android.app.Application
import com.app.thebhangarwale.address.repository.AddressRepositoryImpl
import com.app.thebhangarwale.address.viewmodel.AddressViewModel
import com.app.thebhangarwale.home.request.repository.RequestRepositoryImpl
import com.app.thebhangarwale.home.request.viewmodel.RequestViewModel
import com.app.thebhangarwale.login.viewmodel.LoginViewModel
import com.app.thebhangarwale.home.feed.repository.FeedRepositoryImpl
import com.app.thebhangarwale.home.feed.viewmodel.FeedViewModel
import com.app.thebhangarwale.home.my_account.repository.MyAccountRepositoryImpl
import com.app.thebhangarwale.home.my_account.viewmodel.MyAccountViewModel
import com.app.thebhangarwale.login.repository.LoginRepositoryImpl
import com.app.thebhangarwale.profile.repository.ProfileRepositoryImpl
import com.app.thebhangarwale.profile.viewmodel.ProfileViewModel
import com.app.thebhangarwale.settings.repository.SettingsRepositoryImpl
import com.app.thebhangarwale.settings.viewmodel.SettingsViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import dagger.Module
import dagger.Provides

@Module(includes = [BhangarwaleApplicationModule::class,BhangarwaleRepositoryModule::class])
class BhangarwaleViewModelModule {

    @Provides
    fun provideLoginViewModel(application: Application,loginRepositoryImpl: LoginRepositoryImpl) = LoginViewModel(application,loginRepositoryImpl)

    @Provides
    fun provideFeedViewModel(application: Application,feedRepositoryImpl: FeedRepositoryImpl) = FeedViewModel(application,feedRepositoryImpl)


    //fun provideWhatisNewViewModel(application: Application) = WhatIsNewViewModel(application)

    @Provides
    fun provideMyAccountViewModel(application: Application,myAccountRepositoryImpl: MyAccountRepositoryImpl) = MyAccountViewModel(application,myAccountRepositoryImpl)

    @Provides
    fun provideProfileViewModel(application: Application,profileRepositoryImpl: ProfileRepositoryImpl) = ProfileViewModel(application,profileRepositoryImpl)

    @Provides
    fun provideSettingsViewModel(application: Application,settingsRepositoryImpl: SettingsRepositoryImpl) = SettingsViewModel(application,settingsRepositoryImpl)

    @Provides
    fun provideAddressViewModel(application: Application,addressRepositoryImpl: AddressRepositoryImpl) = AddressViewModel(application,addressRepositoryImpl)

    @Provides
    fun provideRequestViewModel(application: Application,requestRepositoryImpl: RequestRepositoryImpl) = RequestViewModel(application,requestRepositoryImpl)

}