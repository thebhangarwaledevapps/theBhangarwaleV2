package com.app.thebhangarwale.dagger.module

import android.app.Application
import android.location.Geocoder
import com.app.thebhangarwale.address.contract.network.LocationDataSource
import com.app.thebhangarwale.address.datasource.LocationDataSourceImpl
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import dagger.Module
import dagger.Provides

@Module(includes = [BhangarwaleApplicationModule::class])
class BhangarwaleLocationModule {

    @Provides
    fun provideLocationRequest() = LocationRequest.create()

    @Provides
    fun provideLocationSettingsRequest(locationRequest: LocationRequest): LocationSettingsRequest =
        locationRequest.run {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            LocationSettingsRequest.Builder()
                .addLocationRequest(this)
                .setAlwaysShow(true)
                .build()
        }

    @Provides
    fun provideFusedLocationProviderClient(application: Application): FusedLocationProviderClient =
        LocationServices
            .getFusedLocationProviderClient(application.applicationContext)

    @Provides
    fun provideGeocoder(application: Application) = Geocoder(application)

    @Provides
    fun provideSettingsDiscDataSource(
        locationRequest: LocationRequest,
        locationProviderClient: FusedLocationProviderClient,
        geocoder: Geocoder
    ): LocationDataSource = LocationDataSourceImpl(
        locationRequest,
        locationProviderClient,
        geocoder
    )

}