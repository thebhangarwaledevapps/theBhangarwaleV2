package com.app.thebhangarwale.contract.disc.shared_pref

import com.app.thebhangarwale.login.entity.Customer

interface CustomerDetailDiscDataSource {

    @Throws(Exception::class)
    suspend fun saveCustomerIdAndPhoneNumber(
        customerId: String,
        phoneNumber: String,
        countryCode: String
    ) : Customer

    @Throws(Exception::class)
    suspend fun isUserLogin(): Boolean

    @Throws(Exception::class)
    suspend fun getCustomerDetail(): Customer

    @Throws(Exception::class)
    fun getCustomerId() : String

}