package com.app.thebhangarwale.contract.disc.shared_pref

interface SettingsDiscDataSource {

    fun getCurrentLanguage(): String?

    fun isNotificationEnabled(): Boolean

}