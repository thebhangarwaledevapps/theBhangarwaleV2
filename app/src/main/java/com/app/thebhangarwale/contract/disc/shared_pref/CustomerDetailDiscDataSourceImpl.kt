package com.app.thebhangarwale.contract.disc.shared_pref

import android.content.SharedPreferences
import com.app.thebhangarwale.login.entity.Customer

fun SharedPreferences.getStringWithDefaultNullValue(key : String) : String?{
    return getString(key,null)
}

class CustomerDetailDiscDataSourceImpl(val sharedPref: SharedPreferences) : CustomerDetailDiscDataSource {

    override suspend fun saveCustomerIdAndPhoneNumber(
        customerId: String,
        phoneNumber: String,
        countryCode: String
    ) = sharedPref.edit().run {
            putString("customerId", customerId)
            putString("phoneNumber", phoneNumber)
            putString("countryCode",countryCode)
            if (commit()) {
                sharedPref.run {
                    Customer(
                        getStringWithDefaultNullValue("customerId"),
                        getStringWithDefaultNullValue("phoneNumber"),
                        customerCountryCode = getStringWithDefaultNullValue("countryCode")
                    )
                }
            } else {
                throw Exception("Customer didn't save successfully")
            }
        }

    override suspend fun isUserLogin() = sharedPref.run {
        val customerId : String? = getString("customerId",null)
        customerId!=null && customerId.trim().isNotEmpty()
    }

    override suspend fun getCustomerDetail(): Customer {
        val customerId = sharedPref.getStringWithDefaultNullValue("customerId")
        val phoneNumber = sharedPref.getStringWithDefaultNullValue("phoneNumber")
        val customerName = sharedPref.getStringWithDefaultNullValue("customerName")
        val customerProfileImage = sharedPref.getStringWithDefaultNullValue("customerProfileImage")
        val customerCountryCode = sharedPref.getStringWithDefaultNullValue("customerCountryCode")
        return Customer(
            customerId = customerId,
            customerPhoneNumber = phoneNumber,
            customerName = customerName,
            customerProfileImage = customerProfileImage,
            customerCountryCode = customerCountryCode
        )
    }

    override fun getCustomerId(): String {
        return sharedPref.getStringWithDefaultNullValue("customerId") as String
    }

}