package com.app.thebhangarwale.contract.disc.shared_pref

import android.content.SharedPreferences

class SettingsDiscDataSourceImpl(val sharedPref: SharedPreferences) : SettingsDiscDataSource {

    override fun getCurrentLanguage(): String? {
        return sharedPref.getString("theme", "light")
    }

    override fun isNotificationEnabled(): Boolean {
        return sharedPref.getBoolean("notification_enable",true)
    }

}